package controller;

import com.sun.javafx.application.PlatformImpl;

import javafx.application.Platform;
import javafx.stage.Stage;
import view.FXEnvironment;
import view.screens.ClassificaScreenController;
import view.screens.EntrataUscitaScreenController;
import view.screens.GaraScreenController;
import view.screens.ModificheScreenController;
import view.screens.MonopostoScreenController;
import view.screens.PenalitaScreenController;
import view.screens.PersonaleScreenController;
import view.screens.PrincipalScreenController;

public class Main {

    public static void main(String[] args) {
        PlatformImpl.startup(() -> {
        });
        final FXEnvironment environment = new FXEnvironment();
        final PrincipalController principal = new PrincipalController();
        final PrincipalScreenController playerScreen = new PrincipalScreenController(environment, principal);
        final PenalitaController penalita = new PenalitaController();
        final PenalitaScreenController penalitaScreen = new PenalitaScreenController(environment, penalita);
        final ModificheController modifiche = new ModificheController();
        final ModificheScreenController ModificheScreen = new ModificheScreenController(environment, modifiche);
        final GaraController gara = new GaraController();
        final GaraScreenController garaScreen = new GaraScreenController(environment, gara);
        final PersonaleController personale = new PersonaleController();
        final PersonaleScreenController personaleScreen = new PersonaleScreenController(environment, personale);
        final EntrataUscitaController entrataUscitaController = new EntrataUscitaController();
        final EntrataUscitaScreenController entrataUscitaScreenController = new EntrataUscitaScreenController(environment, entrataUscitaController);
        final MonopostoController monopostoController = new MonopostoController();
        final MonopostoScreenController monopostoScreenController = new MonopostoScreenController(environment, monopostoController);
        final ClassificaController classificaController = new ClassificaController();
        final ClassificaScreenController classificaScreenController = new ClassificaScreenController(environment, classificaController);
        
        Platform.runLater(() -> {
            try {
                final Stage primaryStage = new Stage();
                primaryStage.setTitle("Gestionale F1");
                environment.start(primaryStage);
            } catch (Exception e) {
                System.out.println("Unable to load graphic environment.");
                e.printStackTrace();
            }
            playerScreen.show();
        });
    }

}
