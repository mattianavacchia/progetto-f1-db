package model.scuderia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.DBUtil;

public class AssistenteTecnicoDAO {
    
    public static List<AssistenteTecnico> getAssistentiIngegneriList(String scuderia) {
        String query = "SELECT * FROM ASSISTENTE_TECNICO WHERE Tipo = 'Ingegnere' AND Scuderia = '" + scuderia + "';";
        ResultSet rs;
        List<AssistenteTecnico> assistenti = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                AssistenteTecnico a = new AssistenteTecnico();
                a.setCF(rs.getString("CF"));
                a.setNome(rs.getString("Nome"));
                a.setCognome(rs.getString("Cognome"));
                a.setDataNascita(rs.getString("DataNascita"));
                a.setScuderia(rs.getString("Scuderia"));
                a.setTipo(rs.getString("Tipo"));
                assistenti.add(a);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return assistenti;
    }

    public static List<AssistenteTecnico> getAssistentiList() {
        String query = "SELECT * FROM ASSISTENTE_TECNICO";
        ResultSet rs;
        List<AssistenteTecnico> assistenti = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                AssistenteTecnico a = new AssistenteTecnico();
                a.setCF(rs.getString("CF"));
                a.setNome(rs.getString("Nome"));
                a.setCognome(rs.getString("Cognome"));
                a.setDataNascita(rs.getString("DataNascita"));
                a.setScuderia(rs.getString("Scuderia"));
                a.setTipo(rs.getString("Tipo"));
                assistenti.add(a);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return assistenti;
    }
    
    public static void insertAssistenteTecnico(String nome, String cognome, String CF, String dataNascita, String tipo, String scuderia) throws SQLException, ClassNotFoundException {
        String updateStmt = "INSERT INTO ASSISTENTE_TECNICO (CF, Nome, Cognome, DataNascita, Tipo, Scuderia) VALUES ('" + CF + "', '" + nome + "', '" + cognome + "', '" + dataNascita + "', '" + tipo + "', '" + scuderia + "');";
        
        //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }
    
    public static void insertIngegnere(String CF, String tipo) throws SQLException, ClassNotFoundException {
        String updateStmt = "INSERT INTO Ingegnere (CF, Tipo) VALUES ('" + CF + "', '" + tipo + "');";
        
        //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }
    
    public static void deleteAssistenteTecnico(String CF) throws SQLException, ClassNotFoundException {
        //Declare a DELETE statement
        String updateStmt = "DELETE * FROM ASSISTENTE_TECNICO WHERE CF = '" + CF + "';";
        
        //Execute UPDATE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while DELETE Operation: " + e);
            throw e;
        }
    }
    
    public static void deleteIngegnere(String CF) throws SQLException, ClassNotFoundException {
        //Declare a DELETE statement
        String updateStmt = "DELETE * FROM Ingegnere WHERE CF = '" + CF + "';";
        
        //Execute UPDATE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while DELETE Operation: " + e);
            throw e;
        }
    }
}
