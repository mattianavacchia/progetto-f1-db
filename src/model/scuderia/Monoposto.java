package model.scuderia;

public class Monoposto {

    private String cfPilota;
    private int codMonoposto;
    private String telaio;
    private String trasmissione;
    private double peso;
    private double larghezza;
    private double altezza;
    private String dataInizioUtilizzo;
    private String dataFineUtilizzo;
    private String scuderia;
    
    public Monoposto() {
        // TODO Auto-generated constructor stub
    }

    public String getCfPilota() {
        return cfPilota;
    }

    public int getCodMonoposto() {
        return codMonoposto;
    }

    public String getTelaio() {
        return telaio;
    }

    public String getTrasmissione() {
        return trasmissione;
    }

    public double getPeso() {
        return peso;
    }

    public double getLarghezza() {
        return larghezza;
    }

    public double getAltezza() {
        return altezza;
    }

    public String getDataInizioUtilizzo() {
        return dataInizioUtilizzo;
    }

    public String getDataFineUtilizzo() {
        return dataFineUtilizzo;
    }

    public String getScuderia() {
        return scuderia;
    }

    public void setCfPilota(String cfPilota) {
        this.cfPilota = cfPilota;
    }

    public void setCodMonoposto(int codMonoposto) {
        this.codMonoposto = codMonoposto;
    }

    public void setTelaio(String telaio) {
        this.telaio = telaio;
    }

    public void setTrasmissione(String trasmissione) {
        this.trasmissione = trasmissione;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public void setLarghezza(double larghezza) {
        this.larghezza = larghezza;
    }

    public void setAltezza(double altezza) {
        this.altezza = altezza;
    }

    public void setDataInizioUtilizzo(String dataInizioUtilizzo) {
        this.dataInizioUtilizzo = dataInizioUtilizzo;
    }

    public void setDataFineUtilizzo(String dataFineUtilizzo) {
        this.dataFineUtilizzo = dataFineUtilizzo;
    }

    public void setScuderia(String scuderia) {
        this.scuderia = scuderia;
    }
    
    
}
