package model.scuderia;

import java.util.Date;

public class Scuderia {

    private String nome;
    private Date datafondazione;
    private int titoliCostruttori;
    private String p_iva_cpd;
    private int annoCampionato;
    
    public Scuderia() {
        // TODO Auto-generated constructor stub
    }

    public String getNome() {
        return nome;
    }

    public Date getDatafondazione() {
        return datafondazione;
    }

    public int getTitoliCostruttori() {
        return titoliCostruttori;
    }

    public String getP_iva_cpd() {
        return p_iva_cpd;
    }

    public int getAnnoCampionato() {
        return annoCampionato;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDatafondazione(Date datafondazione) {
        this.datafondazione = datafondazione;
    }

    public void setTitoliCostruttori(int titoliCostruttori) {
        this.titoliCostruttori = titoliCostruttori;
    }

    public void setP_iva_cpd(String p_iva_cpd) {
        this.p_iva_cpd = p_iva_cpd;
    }

    public void setAnnoCampionato(int annoCampionato) {
        this.annoCampionato = annoCampionato;
    }
    
    
}
