package model.scuderia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import model.db.DBUtil;

public class PilotaDAO {

    public static List<Pilota> getPilotaList() {
        String query = "SELECT * FROM PILOTA";
        ResultSet rs;
        List<Pilota> piloti = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                Pilota p = new Pilota();
                p.setCF(rs.getString("CF"));
                p.setNome(rs.getString("Nome"));
                p.setCognome(rs.getString("Cognome"));
                p.setPeso(rs.getDouble("Peso"));
                p.setDataDiNascita(rs.getString("DataNascita"));
                p.setRuolo(rs.getString("Ruolo"));
                p.setPunteggio(rs.getInt("Punteggio"));
                p.setSuperLicence(rs.getInt("SuperLicence"));
                piloti.add(p);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return piloti;
    }
    
    public static List<Pilota> getOrderedPilotiList() {
        String query = "SELECT * FROM PILOTA ORDER BY Punteggio";
        ResultSet rs;
        List<Pilota> piloti = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                Pilota p = new Pilota();
                p.setCF(rs.getString("CF"));
                p.setNome(rs.getString("Nome"));
                p.setCognome(rs.getString("Cognome"));
                p.setPeso(rs.getDouble("Peso"));
                p.setDataDiNascita(rs.getString("DataNascita"));
                p.setRuolo(rs.getString("Ruolo"));
                p.setPunteggio(rs.getInt("Punteggio"));
                p.setSuperLicence(rs.getInt("SuperLicence"));
                piloti.add(p);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return piloti;
    }
    
    public static Pilota getPilotaFromCF(String CF) {
        String query = "SELECT * FROM PILOTA WHERE CF = '" + CF + "';";
        ResultSet rs;
        Pilota p = new Pilota();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                p.setCF(rs.getString("CF"));
                p.setNome(rs.getString("Nome"));
                p.setCognome(rs.getString("Cognome"));
                p.setPeso(rs.getDouble("Peso"));
                p.setDataDiNascita(rs.getString("DataNascita"));
                p.setRuolo(rs.getString("Ruolo"));
                p.setPunteggio(rs.getInt("Punteggio"));
                p.setSuperLicence(rs.getInt("SuperLicence"));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return p;
    }
    
    public static void deletePilota(String CF) throws SQLException, ClassNotFoundException {
        //Declare a DELETE statement
        String updateStmt = "DELETE * FROM Pilota WHERE CF = '" + CF + "';";
        
        //Execute UPDATE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while DELETE Operation: " + e);
            throw e;
        }
    }
    
    public static void insertPilota(String nome, String cognome, String CF, String dataNascita, int SuperLicence, int peso, String ruolo, int punteggio) throws SQLException, ClassNotFoundException {
        String updateStmt = "INSERT INTO Pilota (CF, Nome, Cognome, DataNascita, Ruolo, Peso, SuperLicence, Punteggio) VALUES ('" + CF + "', '" + nome + "', '" + cognome + "', '" + dataNascita + "', '" + ruolo + "', " + peso + ", " + SuperLicence + ", " + punteggio + ");";
        
        //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }
    
    public static void aggiornamentoPunteggio(String cf, int punteggio) throws ClassNotFoundException, SQLException {
        String updateStmt = "UPDATE Pilota SET Punteggio = Punteggio + " + punteggio + " WHERE CF = '" + cf +"';";
        
      //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }
}
