package model.scuderia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.DBUtil;

public class ScuderiaDAO {

    public static List<Scuderia> getScuderiaList() {
        String query = "SELECT * FROM SCUDERIA";
        ResultSet rs;
        List<Scuderia> scuderie = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                Scuderia s = new Scuderia();
                s.setNome(rs.getString("Nome"));
                s.setDatafondazione(rs.getDate("DataFondazione"));
                s.setTitoliCostruttori(rs.getInt("TitoliCostruttori"));
                s.setAnnoCampionato(rs.getInt("Anno"));
                scuderie.add(s);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return scuderie;
    }
}
