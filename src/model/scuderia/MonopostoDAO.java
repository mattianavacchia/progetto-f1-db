package model.scuderia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.DBUtil;

public class MonopostoDAO {
    
    public static List<Monoposto> getMonopostoList() {
        String query = "SELECT * FROM MONOPOSTO";
        ResultSet rs;
        List<Monoposto> monoposto = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                Monoposto m = new Monoposto();
                m.setCfPilota(rs.getString("CFPilota"));
                m.setCodMonoposto(rs.getInt("codMonoposto"));
                m.setTelaio(rs.getString("Telaio"));
                m.setTrasmissione(rs.getString("Trasmissione"));
                m.setPeso(rs.getDouble("Peso"));
                m.setLarghezza(rs.getDouble("Larghezza"));
                m.setAltezza(rs.getDouble("Altezza"));
                m.setDataInizioUtilizzo(rs.getString("DataInizioUtilizzo"));
                m.setDataFineUtilizzo(rs.getString("DataDistruzione"));
                m.setScuderia(rs.getString("Scuderia"));
                monoposto.add(m);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return monoposto;
    }
    
    public static void insertMonoposto(String CFPilota, int codMonoposto, String telaio, String trasmissione, double peso, double larghezza, double altezza, String dataInizio, String scuderia) throws SQLException, ClassNotFoundException {
        String updateStmt = "INSERT INTO MONOPOSTO (CFPilota, codMonoposto, Telaio, Trasmissione, Peso, Larghezza, Altezza, DataInizioUtilizzo, Scuderia) VALUES ('" + CFPilota +"', " + codMonoposto + ", '" + telaio + "', '" + trasmissione + "', " + peso + ", " + larghezza + ", " + altezza + ", '" + dataInizio + "', '" + scuderia + "');";
        System.out.println(updateStmt);
        
        //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }
    
    public static void deleteMonoposto(int codMonoposto) throws ClassNotFoundException, SQLException {
        String updateStmt = "DELETE * FROM MONOPOSTO WHERE codMonoposto = " + codMonoposto + ";";
        
        //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }
    
    public static void addDataFineUtilizzo(int codMonoposto, String data) throws ClassNotFoundException, SQLException {
        String updateStmt = "UPDATE MONOPOSTO SET DataDistruzione = '" + data + "' WHERE codMonoposto = " + codMonoposto + ";";
        
      //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }
   
    public static void removeDataFineUtilizzo(int codMonoposto) throws ClassNotFoundException, SQLException {
        String updateStmt = "UPDATE MONOPOSTO SET DataDistruzione = NULL WHERE codMonoposto = " + codMonoposto + ";";
        
      //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }
    
    public static boolean checkMonoposto(String cfPilota) {
        String query = "SELECT * FROM MONOPOSTO WHERE CFPilota = '" + cfPilota + "' AND DataDistruzione IS NULL";
        ResultSet rs;
        List<Monoposto> monoposto = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                Monoposto m = new Monoposto();
                m.setCfPilota(rs.getString("CFPilota"));
                m.setCodMonoposto(rs.getInt("codMonoposto"));
                m.setTelaio(rs.getString("Telaio"));
                m.setTrasmissione(rs.getString("Trasmissione"));
                m.setPeso(rs.getDouble("Peso"));
                m.setLarghezza(rs.getDouble("Larghezza"));
                m.setAltezza(rs.getDouble("Altezza"));
                m.setDataInizioUtilizzo(rs.getString("DataInizioUtilizzo"));
                m.setDataFineUtilizzo(rs.getString("DataDistruzione"));
                m.setScuderia(rs.getString("Scuderia"));
                monoposto.add(m);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return monoposto.size() == 0;
    }
}
