package model.scuderia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.DBUtil;

public class EntrataUscitaDAO {

    public static List<Pilota> getPilotaFromScuderia(String scuderia) {
        String query = "SELECT * FROM ENTRATA_USCITA WHERE Scuderia = '" + scuderia + "';";
        ResultSet rs;
        List<Pilota> piloti = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                Pilota p = PilotaDAO.getPilotaFromCF(rs.getString("CFPilota"));
                piloti.add(p);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return piloti;
    }
    
    public static void insertMovement(String CFPilota, String Scuderia, String data, String e_u) throws SQLException, ClassNotFoundException {
        String updateStmt = "INSERT INTO ENTRATA_USCITA (CFPilota, Scuderia, data, E_U) VALUES ('" + CFPilota + "', '" + Scuderia + "', '" + data + "', '" + e_u + "');";
        
        //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }
    
    public static boolean checkE(String cf) {
        String queryE = "SELECT * FROM ENTRATA_USCITA WHERE CFPilota = '" + cf + "' AND E_U = 'E';";
        ResultSet rsE;
        List<Pilota> pilotiE = new ArrayList<>();
        try {
            rsE = DBUtil.dbExecuteQuery(queryE);
            
            while (rsE.next()) {
                Pilota p = PilotaDAO.getPilotaFromCF(rsE.getString("CFPilota"));
                pilotiE.add(p);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        String queryU = "SELECT * FROM ENTRATA_USCITA WHERE CFPilota = '" + cf + "' AND E_U = 'U';";
        ResultSet rsU;
        List<Pilota> pilotiU = new ArrayList<>();
        try {
            rsU = DBUtil.dbExecuteQuery(queryU);
            
            while (rsU.next()) {
                Pilota p = PilotaDAO.getPilotaFromCF(rsU.getString("CFPilota"));
                pilotiU.add(p);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return pilotiE.size() == pilotiU.size();
    }
    
    public static boolean checkU(String cf, String scuderia) {
        String queryE = "SELECT * FROM ENTRATA_USCITA WHERE CFPilota = '" + cf + "' AND E_U = 'E' AND Scuderia = '" + scuderia + "';";
        ResultSet rsE;
        List<Pilota> pilotiE = new ArrayList<>();
        try {
            rsE = DBUtil.dbExecuteQuery(queryE);
            
            while (rsE.next()) {
                Pilota p = PilotaDAO.getPilotaFromCF(rsE.getString("CFPilota"));
                pilotiE.add(p);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        String queryU = "SELECT * FROM ENTRATA_USCITA WHERE CFPilota = '" + cf + "' AND E_U = 'U' AND Scuderia = '" + scuderia + "';";
        ResultSet rsU;
        List<Pilota> pilotiU = new ArrayList<>();
        try {
            rsU = DBUtil.dbExecuteQuery(queryU);
            
            while (rsU.next()) {
                Pilota p = PilotaDAO.getPilotaFromCF(rsU.getString("CFPilota"));
                pilotiU.add(p);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return pilotiE.size() > pilotiU.size();
    }
}
