package model.scuderia;

public class EntrataUscita {

    private String cfPilota;
    private String scuderia;
    private String data;
    private String e_u;
    
    public EntrataUscita() {
        // TODO Auto-generated constructor stub
    }

    public String getCfPilota() {
        return cfPilota;
    }

    public String getScuderia() {
        return scuderia;
    }

    public String getData() {
        return data;
    }

    public String getE_u() {
        return e_u;
    }

    public void setCfPilota(String cfPilota) {
        this.cfPilota = cfPilota;
    }

    public void setScuderia(String scuderia) {
        this.scuderia = scuderia;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setE_u(String e_u) {
        this.e_u = e_u;
    }
    
    
}
