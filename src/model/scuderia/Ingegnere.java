package model.scuderia;

public class Ingegnere {

    private String CF;
    private String tipo;
    
    public Ingegnere() {
        // TODO Auto-generated constructor stub
    }

    public String getCF() {
        return CF;
    }

    public String getTipo() {
        return tipo;
    }

    public void setCF(String cF) {
        CF = cF;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
