package model.scuderia;

public class Pilota {

    private String CF;
    private String nome;
    private String cognome;
    private String dataDiNascita;
    private int SuperLicence;
    private double peso;
    private String ruolo;
    private int punteggio;
    
    public double getPeso() {
        return this.peso;
    }
    public void setPeso(double peso) {
        this.peso = peso;
    }
    
    public String getCF() {
        return CF;
    }
    public String getNome() {
        return nome;
    }
    public String getCognome() {
        return cognome;
    }
    public String getDataDiNascita() {
        return dataDiNascita;
    }
    public void setCF(String cF) {
        CF = cF;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
    public void setDataDiNascita(String dataDiNascita) {
        this.dataDiNascita = dataDiNascita;
    }
    public int getSuperLicence() {
        return SuperLicence;
    }
    public String getRuolo() {
        return ruolo;
    }
    public int getPunteggio() {
        return punteggio;
    }
    public void setSuperLicence(int superLicence) {
        SuperLicence = superLicence;
    }
    public void setRuolo(String ruolo) {
        this.ruolo = ruolo;
    }
    public void setPunteggio(int punteggio) {
        this.punteggio = punteggio;
    }
    
    
}
