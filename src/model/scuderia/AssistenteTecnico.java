package model.scuderia;

public class AssistenteTecnico {

    private String CF;
    private String nome;
    private String cognome;
    private String dataNascita;
    private String tipo;
    private String scuderia;
    
    public AssistenteTecnico() {
        // TODO Auto-generated constructor stub
    }

    public String getCF() {
        return CF;
    }

    public String getNome() {
        return nome;
    }

    public String getCognome() {
        return cognome;
    }

    public String getDataNascita() {
        return dataNascita;
    }

    public String getTipo() {
        return tipo;
    }

    public String getScuderia() {
        return scuderia;
    }

    public void setCF(String cF) {
        CF = cF;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public void setDataNascita(String dataNascita) {
        this.dataNascita = dataNascita;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setScuderia(String scuderia) {
        this.scuderia = scuderia;
    }
}
