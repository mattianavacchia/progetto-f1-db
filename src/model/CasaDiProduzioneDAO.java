package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.DBUtil;

public class CasaDiProduzioneDAO {

    public static List<CasaDiProduzione> getCasaDiProduzioneList() {
        String query = "SELECT * FROM CASA_DI_PRODUZIONE;" ;
        ResultSet rs;
        List<CasaDiProduzione> list = new ArrayList<>();
        
        try {
            rs = DBUtil.dbExecuteQuery(query);
            while (rs.next()) {
                CasaDiProduzione c = new CasaDiProduzione();
                c.setNome(rs.getString("Nome"));
                c.setP_iva(rs.getString("P_IVA"));
                list.add(c);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return list;
    }
}
