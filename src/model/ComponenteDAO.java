package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.db.DBUtil;
import java.util.*;

public class ComponenteDAO {
    
    public static void creaComponente(String p_iva, String cfPilota, int codMonoposot) throws ClassNotFoundException, SQLException {
        int x = ComponenteDAO.numberComponents() + 1;
        String query = "INSERT INTO COMPONENTE (codComponente, Tipo, P_IVA_CdP, CFPilota, codMonoposto) VALUES (" + x + ", 'PowerUnit', '" + p_iva + "', '" + cfPilota + "', " + codMonoposot + ");";
        
      //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(query);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }
    
    public static void rimuoviComponente(int codMonoposto) throws ClassNotFoundException, SQLException {
        String query = "DELETE FROM COMPONENTE WHERE codMonoposto = " + codMonoposto + ";";
        
      //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(query);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }

    public static int numberComponents() {
        String query = "SELECT * FROM COMPONENTE;";
        ResultSet rs;
        List<Componente> componente = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                Componente c = new Componente();
                c.setCodComponente(rs.getInt("codComponente"));
                componente.add(c);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return componente.size();
    }
    
    public static int codComponenteFromCF(String cf) {
        String query = "SELECT * FROM COMPONENTE WHERE CFPilota = '" + cf + "';" ;
        ResultSet rs;
        Componente componente = null;
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                componente = new Componente();
                componente.setCodComponente(rs.getInt("codComponente"));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return componente != null ? componente.getCodComponente() : -1;
    }
}
