package model;

public class ComponentePU {
    
    private int codComponentePU;
    private String tipo;
    
    public ComponentePU() {
        // TODO Auto-generated constructor stub
    }

    public int getCodComponentePU() {
        return codComponentePU;
    }

    public String getTipo() {
        return tipo;
    }

    public void setCodComponentePU(int codComponentePU) {
        this.codComponentePU = codComponentePU;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    
}
