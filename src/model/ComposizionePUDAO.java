package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.DBUtil;

public class ComposizionePUDAO {
    
    public static void creaComposizione(int codPU, int codComponentePU) throws SQLException, ClassNotFoundException {
        String query = "INSERT INTO COMPOSIZIONE_PU (codPowerUnit, codComponentePU) VALUES (" + codPU + ", '" + codComponentePU + "');";
        //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(query);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }
    
    public static void eliminaComposizione(int codComponentePU) throws SQLException, ClassNotFoundException {
        String query = "DELETE * FROM COMPOSIZIONE_PU WHERE codComponentePU = " + codComponentePU + ";";
        //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(query);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }

    public static List<ComposizionePU> codComponentiPU(int codComponente) {
        String query = "SELECT * FROM COMPOSIZIONE_PU WHERE codPowerUnit = " + codComponente + ";" ;
        ResultSet rs;
        List<ComposizionePU> composizioniPU = new ArrayList<>();
        
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                ComposizionePU c = new ComposizionePU();
                c.setCodComponente(rs.getInt("codComponentePU"));
                c.setCodComponentePU(rs.getInt("codPowerUnit"));
                composizioniPU.add(c);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return composizioniPU;
    }
}
