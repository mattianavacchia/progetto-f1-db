package model.modifihe;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.DBUtil;

public class ModificaDAO {
    
    public static List<Modifica> getModificheList() {
        String query = "SELECT * FROM MODIFICA;";
        ResultSet rs;
        List<Modifica> mods = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                Modifica c = new Modifica();
                c.setData(rs.getString("Data"));
                c.setDescrizione(rs.getString("Descrizione"));
                c.setCodComponente(rs.getInt("codComponente"));
                mods.add(c);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return mods;
    }

    public static void creaMod(String data, String ora, String desc, int codComponente) throws ClassNotFoundException, SQLException {
        String updateStmt = "INSERT INTO MODIFICA (Data, Ora, Descrizione, codComponente) VALUES ('" + data + "', '" + ora + "', '" + desc +  "', " + codComponente + ");";
        System.out.println(updateStmt);
        
        //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }
}
