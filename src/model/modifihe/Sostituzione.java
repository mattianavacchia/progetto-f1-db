package model.modifihe;

public class Sostituzione {

    private String data;
    private int codComponenteAttuale;
    private int codNuovoAttuale;
    
    public Sostituzione() {
        // TODO Auto-generated constructor stub
    }

    public String getData() {
        return data;
    }

    public int getCodComponenteAttuale() {
        return codComponenteAttuale;
    }

    public int getCodNuovoAttuale() {
        return codNuovoAttuale;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setCodComponenteAttuale(int codComponenteAttuale) {
        this.codComponenteAttuale = codComponenteAttuale;
    }

    public void setCodNuovoAttuale(int codNuovoAttuale) {
        this.codNuovoAttuale = codNuovoAttuale;
    }
    
    
    
}
