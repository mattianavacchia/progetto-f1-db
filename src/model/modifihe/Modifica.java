package model.modifihe;

public class Modifica {

    private String data;
    private int codComponente;
    private String descrizione;
    
    public Modifica() {
        // TODO Auto-generated constructor stub
    }

    public String getData() {
        return data;
    }

    public int getCodComponente() {
        return codComponente;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setCodComponente(int codComponente) {
        this.codComponente = codComponente;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }
    
    
}
