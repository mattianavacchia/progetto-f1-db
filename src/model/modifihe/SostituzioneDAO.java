package model.modifihe;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.DBUtil;

public class SostituzioneDAO {
    
    public static List<Sostituzione> getSostituzioniList() {
        String query = "SELECT * FROM SOSTITUZIONE;";
        ResultSet rs;
        List<Sostituzione> sos = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                Sostituzione s = new Sostituzione();
                s.setData(rs.getString("Data"));
                s.setCodComponenteAttuale(rs.getInt("codComponenteAttuale"));
                s.setCodNuovoAttuale(rs.getInt("codComponenteNuovo"));
                sos.add(s);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return sos;
    }

    public static void creaSos(String data, int codComponeteA, int codComponenteNuovo) throws ClassNotFoundException, SQLException {
        //cDate(Format("20130423014854","yyyy-MM-dd hh:mm:ss")
        String updateStmt = "INSERT INTO SOSTITUZIONE (codComponenteAttuale, Data, codComponenteNuovo) VALUES (" + codComponeteA + ", '" + data +  "', " + codComponenteNuovo + ");";
        System.out.println(updateStmt);
        
        //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }
}
