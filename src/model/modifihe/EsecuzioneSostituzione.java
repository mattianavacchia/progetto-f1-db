package model.modifihe;

import java.util.Date;

public class EsecuzioneSostituzione {

    private Date data;
    private String cfIngegnere;
    private int codComponenteAttuale;
    
    public EsecuzioneSostituzione() {
        // TODO Auto-generated constructor stub
    }

    public Date getData() {
        return data;
    }

    public String getCfIngegnere() {
        return cfIngegnere;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public void setCfIngegnere(String cfIngegnere) {
        this.cfIngegnere = cfIngegnere;
    }

    public int getCodComponenteAttuale() {
        return codComponenteAttuale;
    }

    public void setCodComponenteAttuale(int codComponenteAttuale) {
        this.codComponenteAttuale = codComponenteAttuale;
    }
    
}
