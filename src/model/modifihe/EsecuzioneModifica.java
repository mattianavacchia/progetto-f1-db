package model.modifihe;

import java.util.Date;

public class EsecuzioneModifica {

    private Date data;
    private String cfIngegnere;
    
    public EsecuzioneModifica() {
        // TODO Auto-generated constructor stub
    }

    public Date getData() {
        return data;
    }

    public String getCfIngegnere() {
        return cfIngegnere;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public void setCfIngegnere(String cfIngegnere) {
        this.cfIngegnere = cfIngegnere;
    }
    
    
}
