package model.campionato;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.DBUtil;

public class GaraDAO {

    public static List<Gara> getGareList() {
        String query = "SELECT * FROM Gara";
        ResultSet rs;
        List<Gara> gare = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                Gara g = new Gara();
                g.setCitta(rs.getString("Citta"));
                g.setNomeCircuito(rs.getString("NomeCircuito"));
                g.setAnnoCampionato(rs.getInt("Anno"));
                g.setCfPilota(rs.getString("CFPilotaPP"));
                g.setTempoPolePosition(rs.getString("TempoGiroPP"));
                g.setDataOra(rs.getString("DataOra"));
                gare.add(g);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return gare;
    }
    
    public static Gara getGara(String data) {
        String query = "SELECT * FROM Gara WHERE DataOra = '" + data + "';";
        ResultSet rs;
        Gara g = new Gara();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                g.setCfPilota(rs.getString("CFPilotaPP"));
                g.setTempoPolePosition(rs.getString("TempoGiroPP"));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return g;
    }
    
    public static void addPolePosition(String gara, String cf, String time) throws ClassNotFoundException, SQLException {
        String updateStmt = "UPDATE Gara SET CFPilotaPP = '" + cf + "', TempoGiroPP = '" + time + "' WHERE DataOra = '" + gara + "';";
        
        //Execute UPDATE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while DELETE Operation: " + e);
            throw e;
        }
    }
    
}
