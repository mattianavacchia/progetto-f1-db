package model.campionato;

public class PenalitaScuderia {

    private int codPenalita;
    private String descrizione;
    private String Scuderia;
    private int codCorpoGara;
    
    public PenalitaScuderia() {
        // TODO Auto-generated constructor stub
    }

    public int getCodPenalita() {
        return codPenalita;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public String getScuderia() {
        return Scuderia;
    }

    public int getCodCorpoGara() {
        return codCorpoGara;
    }

    public void setCodPenalita(int codPenalita) {
        this.codPenalita = codPenalita;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public void setScuderia(String scuderia) {
        Scuderia = scuderia;
    }

    public void setCodCorpoGara(int codCorpoGara) {
        this.codCorpoGara = codCorpoGara;
    }
    
    
}
