package model.campionato;

public class CorpoGara {
    
    private int codCorpoGara;

    public CorpoGara() {
    }

    public int getCodCorpoGara() {
        return codCorpoGara;
    }

    public void setCodCorpoGara(int codCorpoGara) {
        this.codCorpoGara = codCorpoGara;
    }
}
