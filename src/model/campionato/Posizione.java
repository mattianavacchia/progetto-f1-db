package model.campionato;

public class Posizione {

    private int posizione;
    private int punteggio;
    
    public Posizione() {
        // TODO Auto-generated constructor stub
    }

    public int getPosizione() {
        return posizione;
    }

    public int getPunteggio() {
        return punteggio;
    }

    public void setPosizione(int posizione) {
        this.posizione = posizione;
    }

    public void setPunteggio(int punteggio) {
        this.punteggio = punteggio;
    }
    
    
}
