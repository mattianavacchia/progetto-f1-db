package model.campionato;

public class Campionato {
    
    private String nome;
    private int anno;
    private int codCorpoGara;

    public Campionato() {
    }

    public String getNome() {
        return nome;
    }

    public int getAnno() {
        return anno;
    }

    public int getCodCorpoGara() {
        return codCorpoGara;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public void setCodCorpoGara(int codCorpoGara) {
        this.codCorpoGara = codCorpoGara;
    }

    

}
