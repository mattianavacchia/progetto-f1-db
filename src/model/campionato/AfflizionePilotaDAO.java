package model.campionato;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.DBUtil;
import model.scuderia.Scuderia;

public class AfflizionePilotaDAO {

    public static List<Scuderia> getPenalita(final String codPilota) {
        String query = "SELECT * FROM AFFLIZIONE_PENALITA A, PENALITA P, PILOTI P "
                + "WHERE A.codPenalita = P.codPenalita"
                + "AND A.CFPilota = PI.CF"
                + "AND A.CFPilota = " + codPilota;
        ResultSet rs;
        List<AfflizionePilota> penalita = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                Scuderia s = new Scuderia();
                s.setNome(rs.getString("Nome"));
                s.setDatafondazione(rs.getDate("DataFondazione"));
                s.setTitoliCostruttori(rs.getInt("TitoliCostruttori"));
                s.setP_iva_cpd(rs.getString("P_IVA _dP"));
                s.setAnnoCampionato(rs.getInt("AnnoCampionato"));
                //scuderie.add(s);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
