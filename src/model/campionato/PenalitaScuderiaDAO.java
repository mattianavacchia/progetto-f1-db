package model.campionato;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.DBUtil;

public class PenalitaScuderiaDAO {

    public static List<PenalitaScuderia> getPenalitaScuderieList() {
        String query = "SELECT * FROM PENALITA_SCUDERIA";
        ResultSet rs;
        List<PenalitaScuderia> penalita = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                PenalitaScuderia p = new PenalitaScuderia();
                p.setScuderia(rs.getString("Scuderia"));
                p.setCodCorpoGara(rs.getInt("codCorpoGara"));
                p.setDescrizione(rs.getString("Descrizione"));
                p.setCodPenalita(rs.getInt("codPenalita"));
                penalita.add(p);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return penalita;
    }
    
    public static void deletePenalita(int codPenalita) throws SQLException, ClassNotFoundException {
        //Declare a DELETE statement
        String updateStmt = "DELETE * FROM PENALITA_SCUDERIA WHERE codPenalita = " + codPenalita + ";";
        
        //Execute UPDATE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while DELETE Operation: " + e);
            throw e;
        }
    }
    
    public static void insertPenalita(String descrizione, String Scuderia, int codCorpoGara) throws SQLException, ClassNotFoundException {
        String updateStmt = "INSERT INTO PENALITA_SCUDERIA (Descrizione, Scuderia, codCorpoGara) VALUES ('" + descrizione + "', '" + Scuderia + "', " + codCorpoGara + ");";
        
        //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }
}
