package model.campionato;

public class Gara {
    
    private String dataOra;
    private String citta;
    private String nomeCircuito;
    private int annoCampionato;
    private String tempoPolePosition;
    private String cfPilota;

    public Gara() {
        // TODO Auto-generated constructor stub
    }

    public String getCitta() {
        return citta;
    }

    public String getNomeCircuito() {
        return nomeCircuito;
    }

    public int getAnnoCampionato() {
        return annoCampionato;
    }

    public String getTempoPolePosition() {
        return tempoPolePosition;
    }

    public String getCfPilota() {
        return cfPilota;
    }

    public String getDataOra() {
        return dataOra;
    }

    public void setDataOra(String dataOra) {
        this.dataOra = dataOra;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public void setNomeCircuito(String nomeCircuito) {
        this.nomeCircuito = nomeCircuito;
    }

    public void setAnnoCampionato(int annoCampionato) {
        this.annoCampionato = annoCampionato;
    }

    public void setTempoPolePosition(String tempoPolePosition) {
        this.tempoPolePosition = tempoPolePosition;
    }

    public void setCfPilota(String cfPilota) {
        this.cfPilota = cfPilota;
    }
    
    
}
