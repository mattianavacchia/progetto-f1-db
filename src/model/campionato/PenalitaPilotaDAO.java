package model.campionato;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.DBUtil;

public class PenalitaPilotaDAO {
    
    public static List<PenalitaPilota> getPenalitaPilotiList() {
        String query = "SELECT * FROM PENALITA_PILOTA";
        ResultSet rs;
        List<PenalitaPilota> penalita = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                PenalitaPilota p = new PenalitaPilota();
                p.setCFPilota(rs.getString("CFPilota"));
                p.setCodCorpoGara(rs.getInt("codCorpoGara"));
                p.setDescrizione(rs.getString("Descrizione"));
                p.setCodPenalita(rs.getInt("codPenalita"));
                penalita.add(p);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return penalita;
    }
    
    public static void deletePenalita(int codPenalita) throws SQLException, ClassNotFoundException {
        //Declare a DELETE statement
        String updateStmt = "DELETE * FROM PENALITA_PILOTA WHERE codPenalita = " + codPenalita + ";";
        
        //Execute UPDATE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while DELETE Operation: " + e);
            throw e;
        }
    }
    
    public static void insertPenalita(String descrizione, String CFPilota, int codCorpoGara) throws SQLException, ClassNotFoundException {
        String updateStmt = "INSERT INTO PENALITA_PILOTA (Descrizione, CFPilota, codCorpoGara) VALUES ('" + descrizione + "', '" + CFPilota + "', " + codCorpoGara + ");";
        
        //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }

}
