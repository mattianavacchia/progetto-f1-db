package model.campionato;

public class AfflizionePilota {
    
    private int codPilota;
    private int codPenalita;
    
    public int getCodPilota() {
        return codPilota;
    }
    public int getCodPenalita() {
        return codPenalita;
    }
    public void setCodPilota(int codPilota) {
        this.codPilota = codPilota;
    }
    public void setCodPenalita(int codPenalita) {
        this.codPenalita = codPenalita;
    }
}
