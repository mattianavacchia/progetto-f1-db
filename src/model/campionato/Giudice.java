package model.campionato;

public class Giudice {

    private int codCorpoGara;
    private String CF;
    private String nome;
    private String cognome;
    private String dataNascita;
    
    public Giudice() {
        // TODO Auto-generated constructor stub
    }

    public int getCodCorpoGara() {
        return codCorpoGara;
    }

    public String getCF() {
        return CF;
    }

    public String getNome() {
        return nome;
    }

    public String getCognome() {
        return cognome;
    }

    public String getDataDiNascita() {
        return dataNascita;
    }

    public void setCodCorpoGara(int codCorpoGara) {
        this.codCorpoGara = codCorpoGara;
    }

    public void setCF(String cF) {
        CF = cF;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public void setDataDiNascita(String dataDiNascita) {
        this.dataNascita = dataDiNascita;
    }
}
