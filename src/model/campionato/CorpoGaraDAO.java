package model.campionato;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.DBUtil;

public class CorpoGaraDAO {
    
    public static List<CorpoGara> getCorpoGaraList() {
        String query = "SELECT * FROM CORPO_DI_GARA";
        ResultSet rs;
        List<CorpoGara> corpoGara = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                CorpoGara c = new CorpoGara();
                c.setCodCorpoGara(rs.getInt("codCorpoGara"));
                corpoGara.add(c);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return corpoGara;
    }
}
