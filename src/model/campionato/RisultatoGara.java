package model.campionato;

public class RisultatoGara {

    private String cfPilota;
    private int posizione;
    private String dataOra;
    
    public RisultatoGara() {
        // TODO Auto-generated constructor stub
    }

    public String getCfPilota() {
        return cfPilota;
    }

    public int getPosizione() {
        return posizione;
    }

    public void setCfPilota(String cfPilota) {
        this.cfPilota = cfPilota;
    }

    public void setPosizione(int posizione) {
        this.posizione = posizione;
    }

    public String getDataOra() {
        return dataOra;
    }

    public void setDataOra(String dataOra) {
        this.dataOra = dataOra;
    }
    
    
}
