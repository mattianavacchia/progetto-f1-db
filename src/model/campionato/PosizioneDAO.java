package model.campionato;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.DBUtil;

public class PosizioneDAO {

    public static List<Posizione> getPunteggioFromPosizione(int posizione) {
        String query = "SELECT * FROM Posizione WHERE Posizione = " + posizione;
        ResultSet rs;
        List<Posizione> posizioni = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            while (rs.next()) {
                Posizione p = new Posizione();
                p.setPosizione(rs.getInt("Posizione"));
                p.setPunteggio(rs.getInt("Punteggio"));
                posizioni.add(p);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return posizioni;
    }
}
