package model.campionato;

public class PenalitaPilota {

    private int codPenalita;
    private String descrizione;
    private String CFPilota;
    private int codCorpoGara;
    
    public PenalitaPilota() {
        // TODO Auto-generated constructor stub
    }

    public int getCodPenalita() {
        return codPenalita;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public String getCFPilota() {
        return CFPilota;
    }

    public int getCodCorpoGara() {
        return codCorpoGara;
    }

    public void setCodPenalita(int codPenalita) {
        this.codPenalita = codPenalita;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public void setCFPilota(String cFPilota) {
        CFPilota = cFPilota;
    }

    public void setCodCorpoGara(int codCorpoGara) {
        this.codCorpoGara = codCorpoGara;
    }
    
    
}
