package model.campionato;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.DBUtil;
import model.scuderia.Pilota;
import model.scuderia.PilotaDAO;

public class RisultatoGaraDAO {

    public static void insertRisultato(String gara, String cf, int posizione) throws SQLException, ClassNotFoundException {
        String updateStmt = "INSERT INTO RISULTATO_GARA (DataOra, Posizione, CFPilota) VALUES ('" + gara + "', " + posizione + ", '" + cf + "');";
        
        //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }
    
    public static List<RisultatoGara> getResultsFromGara(String gara) {
        String query = "SELECT * FROM RISULTATO_GARA WHERE DataOra = '" + gara + "';";
        ResultSet rs;
        List<RisultatoGara> risultati = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                RisultatoGara r = new RisultatoGara();
                r.setCfPilota(rs.getString("CFPilota"));
                r.setPosizione(rs.getInt("Posizione"));
                r.setDataOra(rs.getString("DataOra"));
                risultati.add(r);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return risultati;
    }
    
    public static Pilota getPilotaFromRisultato(String gara, int posizione) {
        String query = "SELECT * FROM RISULTATO_GARA WHERE DataOra = '" + gara + "' AND Posizione = " + posizione + ";";
        ResultSet rs;
        List<Pilota> piloti = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                String cf = rs.getString("CFPilota");
                Pilota p = PilotaDAO.getPilotaFromCF(cf);
                piloti.add(p);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(piloti.isEmpty()) {
            return null;
        } else {
            return piloti.get(0);
        }
    }
    

    
    public static void removeFromGara(String gara) throws ClassNotFoundException, SQLException {
        String updateStmt = "DELETE * FROM RISULTATO_GARA WHERE DataOra = '" + gara + "';";
        
        //Execute UPDATE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while DELETE Operation: " + e);
            throw e;
        }
    }
}
