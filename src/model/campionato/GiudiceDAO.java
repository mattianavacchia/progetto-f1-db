package model.campionato;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.DBUtil;

public class GiudiceDAO {

    public static List<Giudice> getGiudiciList() {
        String query = "SELECT * FROM Giudice";
        ResultSet rs;
        List<Giudice> giudici = new ArrayList<>();
        try {
            rs = DBUtil.dbExecuteQuery(query);
            
            while (rs.next()) {
                Giudice g = new Giudice();
                g.setCF(rs.getString("CF"));
                g.setNome(rs.getString("Nome"));
                g.setCodCorpoGara(rs.getInt("codCorpoGara"));
                g.setCognome(rs.getString("Cognome"));
                g.setDataDiNascita(rs.getString("DataNascita"));
                giudici.add(g);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return giudici;
    }
    
    public static void deleteGiuduce(String CF) throws SQLException, ClassNotFoundException {
        //Declare a DELETE statement
        String updateStmt = "DELETE * FROM Giudice WHERE CF = '" + CF + "';";
        
        //Execute UPDATE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while DELETE Operation: " + e);
            throw e;
        }
    }
    
    public static void insertGiudice(String nome, String cognome, String CF, String dataNascita, int codCorpoGara) throws SQLException, ClassNotFoundException {
        String updateStmt = "INSERT INTO Giudice (CF, Nome, Cognome, DataNascita, codCorpoGara) VALUES ('" + CF + "', '" + nome + "', '" + cognome + "', '" + dataNascita + "', " + codCorpoGara + ");";
        
        //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }
}
