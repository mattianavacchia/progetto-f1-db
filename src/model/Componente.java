package model;

public class Componente {

    private int codComponente;
    private String tipo;
    private String cfPilota;
    private int codMonoposto;
    private String p_iva_cdp;
    
    public Componente() {
        // TODO Auto-generated constructor stub
    }

    public int getCodComponente() {
        return codComponente;
    }

    public String getTipo() {
        return tipo;
    }

    public String getCfPilota() {
        return cfPilota;
    }

    public int getCodMonoposto() {
        return codMonoposto;
    }

    public String getP_iva_cdp() {
        return p_iva_cdp;
    }

    public void setCodComponente(int codComponente) {
        this.codComponente = codComponente;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setCfPilota(String cfPilota) {
        this.cfPilota = cfPilota;
    }

    public void setCodMonoposto(int codMonoposto) {
        this.codMonoposto = codMonoposto;
    }

    public void setP_iva_cdp(String p_iva_cdp) {
        this.p_iva_cdp = p_iva_cdp;
    }
    
    
}
