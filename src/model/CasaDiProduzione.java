package model;

public class CasaDiProduzione {

    private String p_iva;
    private String nome;
    
    public CasaDiProduzione() {
        // TODO Auto-generated constructor stub
    }
    
    public String getP_iva() {
        return p_iva;
    }
    public String getNome() {
        return nome;
    }
    public void setP_iva(String p_iva) {
        this.p_iva = p_iva;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    
}
