package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.DBUtil;

public class ComponentePUDAO {
    
    public static int numberOfComponenti() {
        String query = "SELECT * FROM COMPONENTE_PU;" ;
        ResultSet rs;
        List<ComponentePU> list = new ArrayList<>();
        
        try {
            rs = DBUtil.dbExecuteQuery(query);
            while (rs.next()) {
                ComponentePU c = new ComponentePU();
                c.setTipo("Tipo");
                list.add(c);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return list.size();
    }
    
    public static int creaComponente(String tipo) throws ClassNotFoundException, SQLException {
        int cod = ComponentePUDAO.numberOfComponenti() + 1;
        String query = "INSERT INTO COMPONENTE_PU (codComponentePU, Tipo) VALUES (" + cod + ", '" + tipo + "');";
        System.out.println(query);
        
      //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(query);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
        
        return cod;
    }

    public static boolean checkPresenzaComponente(List<ComposizionePU> composizione, String tipo) {
        
        for (ComposizionePU composizionePU : composizione) {
            String query = "SELECT * FROM COMPONENTE_PU WHERE codComponentePU = " + composizionePU.getCodComponente() + ";" ;
            ResultSet rs;
            ComponentePU c = null;
            
            try {
                rs = DBUtil.dbExecuteQuery(query);
                while (rs.next()) {
                    c = new ComponentePU();
                    c.setTipo(rs.getString("Tipo"));
                    if(c.getTipo().equals(tipo)) {
                        return true;
                    }
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            
        }
        
        return false;
    }
    
    public static int codComponente(List<ComposizionePU> composizione, String tipo) {
        
        for (ComposizionePU composizionePU : composizione) {
            String query = "SELECT * FROM COMPONENTE_PU WHERE codComponentePU = " + composizionePU.getCodComponente() + ";" ;
            ResultSet rs;
            ComponentePU c = null;
            
            try {
                rs = DBUtil.dbExecuteQuery(query);
                while (rs.next()) {
                    c = new ComponentePU();
                    c.setCodComponentePU(rs.getInt("codComponentePU"));
                    c.setTipo(rs.getString("Tipo"));
                    if(c.getTipo().equals(tipo)) {
                        return c.getCodComponentePU();
                    }
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            
        }
        
        return -1;
    }
    
    public static void eliminaComponente(int cod) throws ClassNotFoundException, SQLException {
        String query = "DELETE FROM COMPONENTE_PU WHERE codComponentePU = " + cod + ";";
        System.out.println(query);
        
      //Execute DELETE operation
        try {
            DBUtil.dbExecuteUpdate(query);
        } catch (SQLException e) {
            System.out.print("Error occurred while INSERT Operation: " + e);
            throw e;
        }
    }
}
