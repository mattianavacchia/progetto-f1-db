package model;

public class ComposizionePU {
    
    private int codComponentePU;
    private int codComponente;
    
    public ComposizionePU() {
        // TODO Auto-generated constructor stub
    }

    public int getCodComponentePU() {
        return codComponentePU;
    }

    public int getCodComponente() {
        return codComponente;
    }

    public void setCodComponentePU(int codComponentePU) {
        this.codComponentePU = codComponentePU;
    }

    public void setCodComponente(int codComponente) {
        this.codComponente = codComponente;
    }
    
    
}
