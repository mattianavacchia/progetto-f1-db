package view.screens;

/**
 * 
 * @author mattianavach
 *
 */
public enum FXMLScreens {

    /**
     * Screen Principale.
     */
    PRINCIPAL_SCREEN("/view/screens/PrincipalScreen.fxml", "/view/screens/css/PrincipalScreen.css"),
    /**
     * Penalita.
     */
    PENALITA("/view/screens/Penalita.fxml", "/view/screens/Penalita.css"),
    GARA("/view/screens/Gara.fxml", "/view/screens/Gara.css"),
    MODIFICHE("/view/screens/Modifiche.fxml", "/view/screens/Modifiche.css"),
    ENTRATAUSCITA("/view/screens/EntrataUscita.fxml", "/view/screens/EntrataUscita.css"),
    MONOPOSTO("/view/screens/Monoposto.fxml", "/view/screens/Monoposto.css"),
    PERSONALE("/view/screens/Personale.fxml", "/view/screens/Personale.css"),
    CLASSIFICA("/view/screens/Classifica.fxml", "/view/screens/Classifica.css");

    private final String resourcePath;
    private final String cssPath;

    /**
     * @param path
     *            full qualified path of the .fxml
     * @param cssPath
     *            full qualified path of the .css
     */
    FXMLScreens(final String path, final String styleSheetPath) {
        this.resourcePath = path;
        this.cssPath = styleSheetPath;
    }

    /**
     * @return full qualified path of the .fxml
     */
    public String getPath() {
        return resourcePath;
    }

    /**
     * @return full qualified path of the .css
     */
    public String getCssPath() {
        return cssPath;
    }
}
