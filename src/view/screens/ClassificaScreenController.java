package view.screens;

import java.util.Date;

import controller.ClassificaController;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.scuderia.Pilota;
import model.scuderia.PilotaDAO;
import view.FXEnvironment;

public class ClassificaScreenController {
    
    private FXEnvironment environment;
    private ClassificaController controller;
    
    @FXML
    TableView<Pilota> tableViewClassifica;

    public ClassificaScreenController(final FXEnvironment environment, final ClassificaController controller) {
        super();
        this.environment = environment;
        this.environment.loadScreen(FXMLScreens.CLASSIFICA, this);
        this.controller = controller;
    }
    
    @SuppressWarnings("unchecked")
    public void initialize() {
        final TableColumn<Pilota, String> cfPilota = new TableColumn<>("CF");
        cfPilota.setCellValueFactory(
                new PropertyValueFactory<Pilota, String>("CF")
        );
        final TableColumn<Pilota, String> nomePilota = new TableColumn<>("Nome");
        nomePilota.setCellValueFactory(
            new PropertyValueFactory<Pilota, String>("nome")
        );
        final TableColumn<Pilota, String>  cognomePilota = new TableColumn<>("Cognome");
        cognomePilota.setCellValueFactory(
            new PropertyValueFactory<Pilota, String>("cognome")
        );
        final TableColumn<Pilota, Date> dataDiNascitaPilota = new TableColumn<>("Data di Nascita");
        dataDiNascitaPilota.setCellValueFactory(
            new PropertyValueFactory<Pilota, Date>("dataDiNascita")
        );
        final TableColumn<Pilota, Double> pesoPilota = new TableColumn<>("Peso");
        pesoPilota.setCellValueFactory(
            new PropertyValueFactory<Pilota, Double>("peso")
        );
        final TableColumn<Pilota, Integer> superLicencePilota = new TableColumn<>("Super Licence");
        superLicencePilota.setCellValueFactory(
            new PropertyValueFactory<Pilota, Integer>("SuperLicence")
        );
        final TableColumn<Pilota, String> ruoloPilota = new TableColumn<>("Ruolo");
        ruoloPilota.setCellValueFactory(
            new PropertyValueFactory<Pilota, String>("ruolo")
        );
        final TableColumn<Pilota, Integer> punteggioPilota = new TableColumn<>("Punteggio");
        punteggioPilota.setCellValueFactory(
            new PropertyValueFactory<Pilota, Integer>("punteggio")
        );
        //firstNameCol.setSortType(TableColumn.SortType.ASCENDING)
        punteggioPilota.setSortType(TableColumn.SortType.DESCENDING);
        this.tableViewClassifica.getColumns().addAll(cfPilota, nomePilota, cognomePilota, dataDiNascitaPilota, pesoPilota, superLicencePilota, ruoloPilota, punteggioPilota);
        this.tableViewClassifica.setItems(FXCollections.observableArrayList(PilotaDAO.getOrderedPilotiList()));
        //tableView.getSortOrder().add(firstNameCol);
        this.tableViewClassifica.getSortOrder().add(punteggioPilota);
    }
    
    @FXML
    public void tornaAlMenu() {
        this.environment.displayScreen(FXMLScreens.PRINCIPAL_SCREEN);
    }
    
    public void show() {
        this.environment.displayScreen(FXMLScreens.CLASSIFICA);
    }
}