package view.screens;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import controller.MonopostoController;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import model.CasaDiProduzione;
import model.CasaDiProduzioneDAO;
import model.ComponenteDAO;
import model.ComponentePUDAO;
import model.ComposizionePUDAO;
import model.scuderia.Monoposto;
import model.scuderia.MonopostoDAO;
import model.scuderia.Pilota;
import model.scuderia.PilotaDAO;
import model.scuderia.Scuderia;
import model.scuderia.ScuderiaDAO;
import view.FXEnvironment;

public class MonopostoScreenController {
    
    private FXEnvironment environment;
    private MonopostoController controller;
    
    @FXML
    Button btnEliminaMonoposto, btnAggiungiDataFineUtilizzo, btnRimuoviDataFineUtilizzo;
    
    @FXML
    TableView<Monoposto> tableViewMonoposto;
    
    @FXML
    VBox vboxAggiunta;
    
    @FXML
    ComboBox<Pilota> cmbPilota;
    
    @FXML
    ComboBox<Scuderia> cmbScuderia;
    
    @FXML
    ComboBox<CasaDiProduzione> cmbProduttore;
    
    @FXML
    TextField txtTelaio, txtTrasmissione, txtPeso, txtLarghezza, txtAltezza;
    
    public MonopostoScreenController(final FXEnvironment environment, final MonopostoController controller) {
        super();
        this.environment = environment;
        this.environment.loadScreen(FXMLScreens.MONOPOSTO, this);
        this.controller = controller;
    }
    
    @SuppressWarnings("unchecked")
    public void initialize() {
        
        this.cmbProduttore.setCellFactory(param -> new ListCell<CasaDiProduzione>() {
            @Override
            protected void updateItem(final CasaDiProduzione item, final boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null || item.getNome() == null) {
                    setText(null);
                } else {
                    setText(item.getNome());
                }
            }
        });
        
        this.cmbProduttore.setConverter(new StringConverter<CasaDiProduzione>() {
            @Override
            public String toString(CasaDiProduzione person) {
                if (person == null) {
                    return null;
                } else {
                    return person.getNome();
                }
            }

            @Override
            public CasaDiProduzione fromString(String personString) {
                return null; // No conversion fromString needed.
            }
        });
        
        this.cmbScuderia.setCellFactory(param -> new ListCell<Scuderia>() {
            @Override
            protected void updateItem(final Scuderia item, final boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null || item.getNome() == null) {
                    setText(null);
                } else {
                    setText(item.getNome());
                }
            }
        });
        
        this.cmbScuderia.setConverter(new StringConverter<Scuderia>() {
            @Override
            public String toString(Scuderia person) {
                if (person == null) {
                    return null;
                } else {
                    return person.getNome();
                }
            }

            @Override
            public Scuderia fromString(String personString) {
                return null; // No conversion fromString needed.
            }
        });
        
        this.cmbPilota.setCellFactory(param -> new ListCell<Pilota>() {
            @Override
            protected void updateItem(final Pilota item, final boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null || item.getNome() == null || item.getCognome() == null) {
                    setText(null);
                } else {
                    setText(item.getNome() + " " + item.getCognome());
                }
            }
        });
        
        this.cmbPilota.setConverter(new StringConverter<Pilota>() {
            @Override
            public String toString(Pilota pilota) {
                if (pilota == null) {
                    return null;
                } else {
                    return pilota.getNome() + " " + pilota.getCognome();
                }
            }

            @Override
            public Pilota fromString(String personString) {
                return null; // No conversion fromString needed.
            }
        });
        
        // TABELLA
        final TableColumn<Monoposto, String> cfPilotaMonoposto = new TableColumn<>("Pilota");
        cfPilotaMonoposto.setCellValueFactory(
                new PropertyValueFactory<Monoposto, String>("cfPilota")
        );
        final TableColumn<Monoposto, Integer> codMonoposto = new TableColumn<>("Codice");
        codMonoposto.setCellValueFactory(
                new PropertyValueFactory<Monoposto, Integer>("codMonoposto")
        );
        final TableColumn<Monoposto, String> telaioMonoposto = new TableColumn<>("Telaio");
        telaioMonoposto.setCellValueFactory(
                new PropertyValueFactory<Monoposto, String>("telaio")
        );
        final TableColumn<Monoposto, String> trasmissioneMonoposto = new TableColumn<>("Trasmissione");
        trasmissioneMonoposto.setCellValueFactory(
                new PropertyValueFactory<Monoposto, String>("trasmissione")
        );
        final TableColumn<Monoposto, Double> pesoMonoposto = new TableColumn<>("Peso");
        pesoMonoposto.setCellValueFactory(
                new PropertyValueFactory<Monoposto, Double>("peso")
        );
        final TableColumn<Monoposto, Double> larghezzaMonoposto = new TableColumn<>("Larghezza");
        larghezzaMonoposto.setCellValueFactory(
                new PropertyValueFactory<Monoposto, Double>("larghezza")
        );
        final TableColumn<Monoposto, Double> altezzaMonoposto = new TableColumn<>("Altezza");
        altezzaMonoposto.setCellValueFactory(
                new PropertyValueFactory<Monoposto, Double>("altezza")
        );
        final TableColumn<Monoposto, String> dataInizioMonoposto = new TableColumn<>("Data Inizio Utilizzo");
        dataInizioMonoposto.setCellValueFactory(
                new PropertyValueFactory<Monoposto, String>("dataInizioUtilizzo")
        );
        final TableColumn<Monoposto, String> dataFineMonoposto = new TableColumn<>("Data Fine Utilizzo");
        dataFineMonoposto.setCellValueFactory(
                new PropertyValueFactory<Monoposto, String>("dataFineUtilizzo")
        );
        final TableColumn<Monoposto, String> scuderiaMonoposto = new TableColumn<>("Scuderia");
        scuderiaMonoposto.setCellValueFactory(
                new PropertyValueFactory<Monoposto, String>("scuderia")
        );
        
        this.tableViewMonoposto.getColumns().addAll(cfPilotaMonoposto, codMonoposto, telaioMonoposto, trasmissioneMonoposto, pesoMonoposto, larghezzaMonoposto, altezzaMonoposto, dataInizioMonoposto, dataFineMonoposto, scuderiaMonoposto);
    }
    
    public void show() {
        this.environment.displayScreen(FXMLScreens.MONOPOSTO);
    }

    @FXML
    public void eliminaMonoposto() throws ClassNotFoundException, SQLException {
        if(this.tableViewMonoposto.getSelectionModel().getSelectedItem() != null) {
            ComposizionePUDAO.codComponentiPU(ComponenteDAO.codComponenteFromCF(this.tableViewMonoposto.getSelectionModel().getSelectedItem().getCfPilota())).forEach(e -> {
                try {
                    ComponentePUDAO.eliminaComponente(e.getCodComponente());
                } catch (ClassNotFoundException | SQLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            });
            ComposizionePUDAO.eliminaComposizione(ComponenteDAO.codComponenteFromCF(this.tableViewMonoposto.getSelectionModel().getSelectedItem().getCfPilota()));
            ComponenteDAO.rimuoviComponente(this.tableViewMonoposto.getSelectionModel().getSelectedItem().getCodMonoposto());
            MonopostoDAO.deleteMonoposto(this.tableViewMonoposto.getSelectionModel().getSelectedItem().getCodMonoposto());
            this.tableViewMonoposto.setItems(FXCollections.observableArrayList(MonopostoDAO.getMonopostoList()));
        }
    }
    
    @FXML
    public void caricaMonoposto() {
        this.tableViewMonoposto.setItems(FXCollections.observableArrayList(MonopostoDAO.getMonopostoList()));
        this.cmbScuderia.setItems(FXCollections.observableArrayList(ScuderiaDAO.getScuderiaList()));
        this.cmbScuderia.getSelectionModel().select(0);
        this.cmbPilota.setItems(FXCollections.observableArrayList(PilotaDAO.getPilotaList()));
        this.cmbPilota.getSelectionModel().select(0);
        this.cmbProduttore.setItems(FXCollections.observableArrayList(CasaDiProduzioneDAO.getCasaDiProduzioneList()));
        this.cmbProduttore.getSelectionModel().select(0);
        this.btnAggiungiDataFineUtilizzo.setDisable(false);
        this.btnEliminaMonoposto.setDisable(false);
        this.btnRimuoviDataFineUtilizzo.setDisable(false);
        this.vboxAggiunta.setVisible(true);
    }
    
    @FXML
    public void aggiungiDataFineUtilizzo() throws ClassNotFoundException, SQLException {
        if(this.tableViewMonoposto.getSelectionModel().getSelectedItem() != null && this.tableViewMonoposto.getSelectionModel().getSelectedItem().getDataFineUtilizzo() == null) {
            String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            String[] elems = modifiedDate.split("-");
            String dateFinal = elems[2] + "/" + elems[1] + "/" + elems[0];
            if(this.tableViewMonoposto.getSelectionModel().getSelectedItem() != null) {
                MonopostoDAO.addDataFineUtilizzo(this.tableViewMonoposto.getSelectionModel().getSelectedItem().getCodMonoposto(), dateFinal);
            }
        } else {
            final Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Info");
            alert.setHeaderText("Errore Aggiornamento");
            alert.setContentText("Data gia presente.");

            alert.showAndWait();
        }
        
        this.tableViewMonoposto.setItems(FXCollections.observableArrayList(MonopostoDAO.getMonopostoList()));
    }
    
    @FXML
    public void rimuoviDataFineUtilizzo() throws ClassNotFoundException, SQLException {
        if(this.tableViewMonoposto.getSelectionModel().getSelectedItem() != null && this.tableViewMonoposto.getSelectionModel().getSelectedItem().getDataFineUtilizzo() != null) {
            if(this.tableViewMonoposto.getSelectionModel().getSelectedItem() != null) {
                MonopostoDAO.removeDataFineUtilizzo(this.tableViewMonoposto.getSelectionModel().getSelectedItem().getCodMonoposto());
            }
        } else {
            final Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Info");
            alert.setHeaderText("Errore Aggiornamento");
            alert.setContentText("Data non presente.");

            alert.showAndWait();
        }
        
        this.tableViewMonoposto.setItems(FXCollections.observableArrayList(MonopostoDAO.getMonopostoList()));
    }
    
    @FXML
    public void inserisciMonoposto() throws ClassNotFoundException, SQLException {
        String pilota = this.cmbPilota.getSelectionModel().getSelectedItem().getCF();
        int codMonoposto = this.tableViewMonoposto.getItems().size() + 1;
        String telaio = this.txtTelaio.getText().toString();
        String trasmissione = this.txtTrasmissione.getText().toString();
        Double peso = null;
        if(this.txtPeso.getText().length() > 0) {
            peso = Double.valueOf(this.txtPeso.getText());
        }
        Double larghezza = null;
        if(this.txtLarghezza.getText().length() > 0) {
            larghezza = Double.valueOf(this.txtLarghezza.getText());
        }
        Double altezza = null;
        if(this.txtAltezza.getText().length() > 0) {
            altezza = Double.valueOf(this.txtAltezza.getText());
        }
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String[] elems = modifiedDate.split("-");
        String dateFinal = elems[2] + "/" + elems[1] + "/" + elems[0];
        String scuderia = this.cmbScuderia.getSelectionModel().getSelectedItem().getNome();
        
        if(MonopostoDAO.checkMonoposto(pilota)) {
            if(telaio.trim().length() > 0 && trasmissione.trim().length() > 0 && peso.toString().trim().length() > 0 && larghezza.toString().trim().length() > 0 && altezza.toString().trim().length() > 0) {
                MonopostoDAO.insertMonoposto(pilota, codMonoposto, telaio, trasmissione, peso, larghezza, altezza, dateFinal, scuderia);
                ComponenteDAO.creaComponente(this.cmbProduttore.getSelectionModel().getSelectedItem().getP_iva(), pilota, codMonoposto);
                this.tableViewMonoposto.setItems(FXCollections.observableArrayList(MonopostoDAO.getMonopostoList()));
            } else {
                final Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Info");
                alert.setHeaderText("Errore Inserimento");
                alert.setContentText("Completare i campi in modo corretto.");

                alert.showAndWait();
            }
        } else {
            final Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Info");
            alert.setHeaderText("Errore Inserimento");
            alert.setContentText("Il pilota ha gia una macchina associata senza data di fine utilizzo.");

            alert.showAndWait();
        }
    }
    
    @FXML
    public void tornaAlMenu() {
        this.environment.displayScreen(FXMLScreens.PRINCIPAL_SCREEN);
    }
}