package view.screens;

import view.FXEnvironment;

import java.sql.SQLException;
import java.util.*;

import controller.GaraController;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import model.campionato.Gara;
import model.campionato.GaraDAO;
import model.campionato.PosizioneDAO;
import model.campionato.RisultatoGara;
import model.campionato.RisultatoGaraDAO;
import model.scuderia.Pilota;
import model.scuderia.PilotaDAO;

public class GaraScreenController {

    private FXEnvironment environment;
    private GaraController controller;
    private List<ComboBox<Pilota>> listCombo = new ArrayList<>();
    
    @FXML
    ComboBox<Pilota> cmbPos1, cmbPos2, cmbPos3, cmbPos4, cmbPos5, cmbPos6, cmbPos7, cmbPos8, cmbPos9, cmbPos10, cmbPos11, cmbPos12, cmbPos13, cmbPos14, cmbPos15, cmbPos16, cmbPos17, cmbPos18, cmbPos19, cmbPos20;
    
    @FXML
    ComboBox<Gara> cmbGara;
    
    @FXML
    TableView<RisultatoGara> tableViewRisultati;
    
    @FXML
    Text txtPole;
    
    @FXML
    VBox vboxContenuto;
    
    public GaraScreenController(final FXEnvironment environment, final GaraController controller) {
        super();
        this.environment = environment;
        this.environment.loadScreen(FXMLScreens.GARA, this);
        this.controller = controller;
    }
    
    @SuppressWarnings("unchecked")
    public void initialize() {
        
        final TableColumn<RisultatoGara, String> gara = new TableColumn<>("Data Gara");
        gara.setCellValueFactory(
                new PropertyValueFactory<RisultatoGara, String>("dataOra")
        );
        final TableColumn<RisultatoGara, String> pilota = new TableColumn<>("CF Pilota");
        pilota.setCellValueFactory(
                new PropertyValueFactory<RisultatoGara, String>("cfPilota")
        );
        final TableColumn<RisultatoGara, Integer> posizione = new TableColumn<>("Posizione");
        posizione.setCellValueFactory(
                new PropertyValueFactory<RisultatoGara, Integer>("posizione")
        );
        this.tableViewRisultati.getColumns().addAll(gara, pilota, posizione);
        
        this.cmbGara.setCellFactory(param -> new ListCell<Gara>() {
            @Override
            protected void updateItem(final Gara item, final boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null || item.getAnnoCampionato() == 0 || item.getCitta() == null || item.getNomeCircuito() == null) {
                    setText(null);
                } else {
                    setText(item.getDataOra() + " - " + item.getCitta() + " " + item.getNomeCircuito());
                }
            }
        });
        
        this.cmbGara.setConverter(new StringConverter<Gara>() {
            @Override
            public String toString(Gara item) {
                if (item == null) {
                    return null;
                } else {
                    return item.getCitta() + " " + item.getNomeCircuito();
                }
            }

            @Override
            public Gara fromString(String personString) {
                return null; // No conversion fromString needed.
            }
        });
        
        this.cmbGara.setItems(FXCollections.observableArrayList(GaraDAO.getGareList()));
        this.cmbGara.getSelectionModel().select(0);
        
        // popolazione lista di combo
        this.listCombo.add(cmbPos1);
        this.listCombo.add(cmbPos2);
        this.listCombo.add(cmbPos3);
        this.listCombo.add(cmbPos4);
        this.listCombo.add(cmbPos5);
        this.listCombo.add(cmbPos6);
        this.listCombo.add(cmbPos7);
        this.listCombo.add(cmbPos8);
        this.listCombo.add(cmbPos9);
        this.listCombo.add(cmbPos10);
        this.listCombo.add(cmbPos11);
        this.listCombo.add(cmbPos12);
        this.listCombo.add(cmbPos13);
        this.listCombo.add(cmbPos14);
        this.listCombo.add(cmbPos15);
        this.listCombo.add(cmbPos16);
        this.listCombo.add(cmbPos17);
        this.listCombo.add(cmbPos18);
        this.listCombo.add(cmbPos19);
        this.listCombo.add(cmbPos20);
        
        this.listCombo.forEach(c -> {
            c.setCellFactory(param -> new ListCell<Pilota>() {
                @Override
                protected void updateItem(final Pilota item, final boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null || item.getNome() == null || item.getCognome() == null) {
                        setText(null);
                    } else {
                        setText(item.getNome() + " " + item.getCognome());
                    }
                }
            });
            
            c.setConverter(new StringConverter<Pilota>() {
                @Override
                public String toString(Pilota pilota) {
                    if (pilota == null) {
                        return null;
                    } else {
                        return pilota.getNome() + " " + pilota.getCognome();
                    }
                }

                @Override
                public Pilota fromString(String personString) {
                    return null; // No conversion fromString needed.
                }
            });
        });
        
        this.listCombo.forEach(c -> c.setItems(FXCollections.observableArrayList(PilotaDAO.getPilotaList())));
    }
    
    @FXML
    public void caricaGara() {
        //Pole Position: NON DISPONIBILE - Tempo: NON DISPONIBILE
        Gara g = GaraDAO.getGara(this.cmbGara.getSelectionModel().getSelectedItem().getDataOra());
        Pilota p = PilotaDAO.getPilotaFromCF(g.getCfPilota());
        if(p.getNome() != null && p.getCognome() != null) {
            this.txtPole.setText("Pole Position: " + p.getNome() + " " + p.getCognome() + " - Tempo: " + g.getTempoPolePosition());
        }
        if(!RisultatoGaraDAO.getResultsFromGara(this.cmbGara.getSelectionModel().getSelectedItem().getDataOra()).isEmpty()) {
            this.tableViewRisultati.setItems(FXCollections.observableArrayList(RisultatoGaraDAO.getResultsFromGara(this.cmbGara.getSelectionModel().getSelectedItem().getDataOra())));
            for(int i = 0; i < this.listCombo.size(); i++) {
                if(RisultatoGaraDAO.getPilotaFromRisultato(this.cmbGara.getSelectionModel().getSelectedItem().getDataOra(), i + 1) != null) {
                    String cfPilota = RisultatoGaraDAO.getPilotaFromRisultato(this.cmbGara.getSelectionModel().getSelectedItem().getDataOra(), i + 1).getCF();
                    for(int k = 0; k < this.listCombo.get(i).getItems().size(); k++) {
                        if(listCombo.get(i).getItems().get(k).getCF().equals(cfPilota)) {
                            this.listCombo.get(i).getSelectionModel().select(k);
                        }
                    }
                } else {
                    this.listCombo.get(i).getSelectionModel().clearSelection();
                }
            }
        } else {
            this.tableViewRisultati.setItems(FXCollections.observableArrayList(RisultatoGaraDAO.getResultsFromGara(this.cmbGara.getSelectionModel().getSelectedItem().getDataOra())));
        }
        this.vboxContenuto.setVisible(true);
    }
    
    @FXML
    public void aggiungiPP() {
        final Stage addStage = new Stage();
        final VBox content = new VBox();
        Scene stageScene = null;
        
        // CF
        final HBox contentCF = new HBox();
        final Label labelCF = new Label("CF: ");
        final ComboBox<Pilota> piloti = new ComboBox<>();
        piloti.setItems(FXCollections.observableArrayList(PilotaDAO.getPilotaList()));
        piloti.setCellFactory(param -> new ListCell<Pilota>() {
            @Override
            protected void updateItem(final Pilota item, final boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null || item.getNome() == null || item.getCognome() == null) {
                    setText(null);
                } else {
                    setText(item.getNome() + " " + item.getCognome());
                }
            }
        });
        
        piloti.setConverter(new StringConverter<Pilota>() {
            @Override
            public String toString(Pilota pilota) {
                if (pilota == null) {
                    return null;
                } else {
                    return pilota.getNome() + " " + pilota.getCognome();
                }
            }

            @Override
            public Pilota fromString(String personString) {
                return null; // No conversion fromString needed.
            }
        });
        contentCF.getChildren().addAll(labelCF, piloti);
        // TEMPO
        final HBox contentTempo = new HBox();
        final Label labelTempo = new Label("TEMPO: ");
        final TextField Tempo = new TextField();
        contentTempo.getChildren().addAll(labelTempo, Tempo);
        
        Gara g = GaraDAO.getGara(this.cmbGara.getSelectionModel().getSelectedItem().getDataOra());
        Pilota p = PilotaDAO.getPilotaFromCF(g.getCfPilota());
        if(p.getCF() != null) {
            Tempo.setText(g.getTempoPolePosition());
            for(int k = 0; k < piloti.getItems().size(); k++) {
                if(piloti.getItems().get(k).getCF().equals(p.getCF())) {
                    piloti.getSelectionModel().select(k);
                }
            }
        }
        
        final Button buttonCommit = new Button("Commit");
        buttonCommit.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(final ActionEvent e) {
                try {
                    GaraDAO.addPolePosition(cmbGara.getSelectionModel().getSelectedItem().getDataOra(), piloti.getSelectionModel().getSelectedItem().getCF(), Tempo.getText().toString());
                    Gara g = GaraDAO.getGara(cmbGara.getSelectionModel().getSelectedItem().getDataOra());
                    Pilota p = PilotaDAO.getPilotaFromCF(g.getCfPilota());
                    if(p.getNome() != null && p.getCognome() != null) {
                        txtPole.setText("Pole Position: " + p.getNome() + " " + p.getCognome() + " - Tempo: " + g.getTempoPolePosition());
                    }
                    addStage.close();
                } catch (ClassNotFoundException | SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });

        stageScene = new Scene(content, 300, 270);
        content.getChildren().addAll(contentCF, contentTempo, buttonCommit);
        addStage.setScene(stageScene);
        addStage.show();
    }
    
    @FXML
    public void salva() throws ClassNotFoundException, SQLException {
        String gara = this.cmbGara.getSelectionModel().getSelectedItem().getDataOra();
        RisultatoGaraDAO.removeFromGara(gara);
        if(!checkErrore() && !checkDuplicati() && !checkAllNull()) {
            for(int i = 0; i < this.listCombo.size(); i++) {
                if(this.listCombo.get(i).getSelectionModel().getSelectedItem() != null) {
                    String cf = this.listCombo.get(i).getSelectionModel().getSelectedItem().getCF();
                    PilotaDAO.aggiornamentoPunteggio(cf, PosizioneDAO.getPunteggioFromPosizione(i + 1).get(0).getPunteggio());
                    RisultatoGaraDAO.insertRisultato(gara, cf, i + 1);
                    this.tableViewRisultati.setItems(FXCollections.observableArrayList(RisultatoGaraDAO.getResultsFromGara(this.cmbGara.getSelectionModel().getSelectedItem().getDataOra())));
                }
            }
        } else {
            final Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Info");
            alert.setHeaderText("Errore Inserimento");
            alert.setContentText("Completare i campi in modo corretto.");

            alert.showAndWait();
        }
    }
    
    private boolean checkAllNull() {
        boolean res = true;
        for (ComboBox<Pilota> comboBox : listCombo) {
            if(comboBox.getSelectionModel().getSelectedItem() != null) {
                res = false;
            }
        }
        return res;
    }
    
    private boolean checkDuplicati() {
        int f = 0;
        for(int i = 0; i < this.listCombo.size(); i++) {
            for(int k = 0; k < this.listCombo.size(); k++) {
                if(this.listCombo.get(i).getSelectionModel().getSelectedItem() != null && this.listCombo.get(k).getSelectionModel().getSelectedItem() != null) {
                    if(this.listCombo.get(i).getSelectionModel().getSelectedItem().getCF().equals(this.listCombo.get(k).getSelectionModel().getSelectedItem().getCF())) {
                        f++;
                    }
                }
            }
        }
        
        return (f % 2) != 0;
    }
    
    private boolean checkErrore() {
        boolean checkErrore = false;
        boolean nullo = false;
        for (ComboBox<Pilota> comboBox : listCombo) {
            if(comboBox.getSelectionModel().getSelectedItem() != null && nullo == true) {
                checkErrore = true;
                break;
            }
            if(comboBox.getSelectionModel().getSelectedItem() == null) {
                nullo = true;
            }
        }
        
        return checkErrore;
    }
    
    public void show() {
        this.environment.displayScreen(FXMLScreens.GARA);
    }
    
    @FXML
    public void tornaAlMenu() {
        this.environment.displayScreen(FXMLScreens.PRINCIPAL_SCREEN);
    }
}
