package view.screens;

import view.FXEnvironment;

import java.sql.SQLException;
import java.util.Date;

import controller.PersonaleController;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.campionato.CorpoGaraDAO;
import model.campionato.Giudice;
import model.campionato.GiudiceDAO;
import model.scuderia.AssistenteTecnico;
import model.scuderia.AssistenteTecnicoDAO;
import model.scuderia.Pilota;
import model.scuderia.PilotaDAO;
import model.scuderia.ScuderiaDAO;

public class PersonaleScreenController {

    private FXEnvironment environment;
    private PersonaleController controller;
    
    @FXML
    CheckBox ckbGiudice, ckbScuderia;
    
    @FXML
    ComboBox<String> cmbScuderia;
    
    @FXML
    TableView<Giudice> tableViewGiudici;
    
    @FXML
    TableView<Pilota> tableViewPiloti;
    
    @FXML
    TableView<AssistenteTecnico> tableViewAssistenti;
    
    @FXML
    HBox hboxAzioni;

    public PersonaleScreenController(final FXEnvironment environment, final PersonaleController controller) {
        super();
        this.environment = environment;
        this.environment.loadScreen(FXMLScreens.PERSONALE, this);
        this.controller = controller;
    }
    
    @SuppressWarnings("unchecked")
    public void initialize() {
        this.cmbScuderia.getItems().addAll("Pilota", "AssistenteTecnico");
        this.cmbScuderia.getSelectionModel().select(0);
        
        /**********************
         * PILOTI
         * *******************/
        final TableColumn<Pilota, String> cfPilota = new TableColumn<>("CF");
        cfPilota.setCellValueFactory(
                new PropertyValueFactory<Pilota, String>("CF")
        );
        final TableColumn<Pilota, String> nomePilota = new TableColumn<>("Nome");
        nomePilota.setCellValueFactory(
            new PropertyValueFactory<Pilota, String>("nome")
        );
        final TableColumn<Pilota, String>  cognomePilota = new TableColumn<>("Cognome");
        cognomePilota.setCellValueFactory(
            new PropertyValueFactory<Pilota, String>("cognome")
        );
        final TableColumn<Pilota, Date> dataDiNascitaPilota = new TableColumn<>("Data di Nascita");
        dataDiNascitaPilota.setCellValueFactory(
            new PropertyValueFactory<Pilota, Date>("dataDiNascita")
        );
        final TableColumn<Pilota, Double> pesoPilota = new TableColumn<>("Peso");
        pesoPilota.setCellValueFactory(
            new PropertyValueFactory<Pilota, Double>("peso")
        );
        final TableColumn<Pilota, Integer> superLicencePilota = new TableColumn<>("Super Licence");
        superLicencePilota.setCellValueFactory(
            new PropertyValueFactory<Pilota, Integer>("SuperLicence")
        );
        final TableColumn<Pilota, String> ruoloPilota = new TableColumn<>("Ruolo");
        ruoloPilota.setCellValueFactory(
            new PropertyValueFactory<Pilota, String>("ruolo")
        );
        final TableColumn<Pilota, Integer> punteggioPilota = new TableColumn<>("Punteggio");
        punteggioPilota.setCellValueFactory(
            new PropertyValueFactory<Pilota, Integer>("punteggio")
        );
        this.tableViewPiloti.getColumns().addAll(cfPilota, nomePilota, cognomePilota, dataDiNascitaPilota, pesoPilota, superLicencePilota, ruoloPilota, punteggioPilota);
        
        /**********************
         * GIUDICI
         * *******************/
        final TableColumn<Giudice, String> cfGiudice = new TableColumn<>("CF");
        cfGiudice.setCellValueFactory(
                new PropertyValueFactory<Giudice, String>("CF")
        );
        final TableColumn<Giudice, String> nomeGiudice = new TableColumn<>("Nome");
        nomeGiudice.setCellValueFactory(
            new PropertyValueFactory<Giudice, String>("nome")
        );
        final TableColumn<Giudice, String>  cognomeGiudice = new TableColumn<>("Cognome");
        cognomeGiudice.setCellValueFactory(
            new PropertyValueFactory<Giudice, String>("cognome")
        );
        final TableColumn<Giudice, Date> dataDiNascitaGiudice = new TableColumn<>("Data di Nascita");
        dataDiNascitaGiudice.setCellValueFactory(
            new PropertyValueFactory<Giudice, Date>("dataDiNascita")
        );
        final TableColumn<Giudice, Date> codCorpoGaraGiudice = new TableColumn<>("Codice Corpo Gara");
        codCorpoGaraGiudice.setCellValueFactory(
            new PropertyValueFactory<Giudice, Date>("codCorpoGara")
        );
        this.tableViewGiudici.getColumns().addAll(cfGiudice, nomeGiudice, cognomeGiudice, dataDiNascitaGiudice, codCorpoGaraGiudice);
        
        /*******************************
         * ASSISTENTI
         *******************************/
        final TableColumn<AssistenteTecnico, String> cfAssistente = new TableColumn<>("CF");
        cfAssistente.setCellValueFactory(
                new PropertyValueFactory<AssistenteTecnico, String>("CF")
        );
        final TableColumn<AssistenteTecnico, String> nomeAssistente = new TableColumn<>("Nome");
        nomeAssistente.setCellValueFactory(
            new PropertyValueFactory<AssistenteTecnico, String>("nome")
        );
        final TableColumn<AssistenteTecnico, String>  cognomeAssistente = new TableColumn<>("Cognome");
        cognomeAssistente.setCellValueFactory(
            new PropertyValueFactory<AssistenteTecnico, String>("cognome")
        );
        final TableColumn<AssistenteTecnico, Date> dataDiNascitaAssistente = new TableColumn<>("Data di Nascita");
        dataDiNascitaAssistente.setCellValueFactory(
            new PropertyValueFactory<AssistenteTecnico, Date>("dataNascita")
        );
        final TableColumn<AssistenteTecnico, String> tipoAssistente = new TableColumn<>("Tipo");
        tipoAssistente.setCellValueFactory(
            new PropertyValueFactory<AssistenteTecnico, String>("tipo")
        );
        final TableColumn<AssistenteTecnico, String> scuderiaAssistente = new TableColumn<>("Scuderia");
        scuderiaAssistente.setCellValueFactory(
            new PropertyValueFactory<AssistenteTecnico, String>("scuderia")
        );
        this.tableViewAssistenti.getColumns().addAll(cfAssistente, nomeAssistente, dataDiNascitaAssistente, tipoAssistente, scuderiaAssistente);
    }
    
    @FXML
    public void carica() {
        this.hboxAzioni.setVisible(true);
        if(this.ckbGiudice.isSelected()) {
            // GIUDICI
            this.tableViewGiudici.setVisible(true);
            this.tableViewPiloti.setVisible(false);
            this.tableViewAssistenti.setVisible(false);
            this.tableViewGiudici.setItems(FXCollections.observableArrayList(GiudiceDAO.getGiudiciList()));
        } else {
            if(this.cmbScuderia.getSelectionModel().getSelectedIndex() == 0) {
                // PILOTA
                this.tableViewGiudici.setVisible(false);
                this.tableViewPiloti.setVisible(true);
                this.tableViewAssistenti.setVisible(false);
                this.tableViewPiloti.setItems(FXCollections.observableArrayList(PilotaDAO.getPilotaList()));
            } else {
                // ASSISTENTE TECNICO
                this.tableViewGiudici.setVisible(false);
                this.tableViewPiloti.setVisible(false);
                this.tableViewAssistenti.setVisible(true);
                this.tableViewAssistenti.setItems(FXCollections.observableArrayList(AssistenteTecnicoDAO.getAssistentiList()));
            }
        }
    }
    
    @FXML
    public void aggiungi() {
        final Stage addStage = new Stage();
        final VBox content = new VBox();
        Scene stageScene = null;
        
        // CF
        final HBox contentCF = new HBox();
        final Label labelCF = new Label("CF: ");
        final TextField CF = new TextField();
        contentCF.getChildren().addAll(labelCF, CF);
        // Nome
        final HBox contentNome = new HBox();
        final Label labelNome = new Label("Nome: ");
        final TextField Nome = new TextField();
        contentNome.getChildren().addAll(labelNome, Nome);
        // Cognome
        final HBox contentCognome = new HBox();
        final Label labelCognome = new Label("Cognome: ");
        final TextField Cognome = new TextField();
        contentCognome.getChildren().addAll(labelCognome, Cognome);
        // DataDiNascita
        final HBox contentDataNascita = new HBox();
        final Label labelDataNascita = new Label("Data Nascita: ");
        final DatePicker dataNascita = new DatePicker();
        contentDataNascita.getChildren().addAll(labelDataNascita, dataNascita);
        
        if(this.ckbGiudice.isSelected()) {
            stageScene = new Scene(content, 280, 200);
            // codCorpoGara
            final HBox contentCodCordoGara = new HBox();
            final Label labelCodCordoGara = new Label("Corpo Gara: ");
            final ComboBox<Integer> cmbCCG = new ComboBox<>();
            CorpoGaraDAO.getCorpoGaraList().forEach(c -> cmbCCG.getItems().add(c.getCodCorpoGara()));
            cmbCCG.getSelectionModel().select(0);
            contentCodCordoGara.getChildren().addAll(labelCodCordoGara, cmbCCG);
            
            final Button buttonCommit = new Button("Commit");
            buttonCommit.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(final ActionEvent e) {
                    try {
                        if(Nome.getText().toString().trim().length() > 0 && Cognome.getText().toString().trim().length() > 0 && CF.getText().toString().trim().length() > 0) {
                            String[] elems = dataNascita.getValue().toString().split("-");
                            String dateFinal = elems[2] + "/" + elems[1] + "/" + elems[0];
                            GiudiceDAO.insertGiudice(Nome.getText().toString(), Cognome.getText().toString(), CF.getText().toString().toUpperCase(), dateFinal, cmbCCG.getSelectionModel().getSelectedItem().intValue());
                            tableViewGiudici.setItems(FXCollections.observableArrayList(GiudiceDAO.getGiudiciList()));
                            addStage.close();
                        } else {
                            final Alert alert = new Alert(AlertType.INFORMATION);
                            alert.setTitle("Info");
                            alert.setHeaderText("Errore Inserimento Giudice");
                            alert.setContentText("Inserire campi in modo corretto");

                            alert.showAndWait();
                        }
                    } catch (ClassNotFoundException | SQLException e1) {
                        e1.printStackTrace();
                    }
                }
            });
            
            content.getChildren().addAll(contentCF, contentNome, contentCognome, contentDataNascita, contentCodCordoGara, buttonCommit);
        } else {
            if(this.cmbScuderia.getSelectionModel().getSelectedIndex() == 0) {
                stageScene = new Scene(content, 300, 270);
                // Pilota
                //SuperLicence
                final HBox contentSuperLicence = new HBox();
                final Label labelSuperLicence = new Label("SuperLicence: ");
                final TextField SuperLicence = new TextField();
                contentSuperLicence.getChildren().addAll(labelSuperLicence, SuperLicence);
                // peso
                final HBox contentPeso = new HBox();
                final Label labelPeso = new Label("Peso: ");
                final TextField Peso = new TextField();
                contentPeso.getChildren().addAll(labelPeso, Peso);
                // ruolo
                final HBox contentRuolo = new HBox();
                final Label labelRuolo = new Label("Ruolo: ");
                final ComboBox<String> cmbRuolo = new ComboBox<>();
                cmbRuolo.getItems().addAll("PrimoPilota", "TerzoPilota");
                cmbRuolo.getSelectionModel().select(0);
                contentRuolo.getChildren().addAll(labelRuolo, cmbRuolo);
                //SuperLicence
                final HBox contentPunteggio = new HBox();
                final Label labelPunteggio = new Label("Punteggio: ");
                final TextField Punteggio = new TextField();
                contentPunteggio.getChildren().addAll(labelPunteggio, Punteggio);
                
                final Button buttonCommit = new Button("Commit");
                buttonCommit.setOnAction(new EventHandler<ActionEvent>() {
                    @Override public void handle(final ActionEvent e) {
                        try {
                            if(Nome.getText().toString().trim().length() > 0 && Cognome.getText().toString().trim().length() > 0 && CF.getText().toString().trim().length() > 0) {
                                String[] elems = dataNascita.getValue().toString().split("-");
                                String dateFinal = elems[2] + "/" + elems[1] + "/" + elems[0];
                                PilotaDAO.insertPilota(Nome.getText().toString(), Cognome.getText().toString(), CF.getText().toString().toUpperCase(), dateFinal, Integer.parseInt(SuperLicence.getText().toString()), Integer.parseInt(Peso.getText().toString()), cmbRuolo.getSelectionModel().getSelectedItem().toString(), Integer.parseInt(Punteggio.getText().toString()));
                                tableViewPiloti.setItems(FXCollections.observableArrayList(PilotaDAO.getPilotaList()));
                                addStage.close();
                            } else {
                                final Alert alert = new Alert(AlertType.INFORMATION);
                                alert.setTitle("Info");
                                alert.setHeaderText("Errore Inserimento Pilota");
                                alert.setContentText("Inserire campi in modo corretto");

                                alert.showAndWait();
                            }
                        } catch (NumberFormatException | ClassNotFoundException | SQLException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                        addStage.close();
                    }
                });
                
                content.getChildren().addAll(contentCF, contentNome, contentCognome, contentDataNascita, contentSuperLicence, contentPeso, contentRuolo, contentPunteggio, buttonCommit);
            } else {
                // Assistente
                stageScene = new Scene(content, 300, 270);
                // ruolo
                final HBox contentRuolo = new HBox();
                final Label labelRuolo = new Label("Ruolo: ");
                final ComboBox<String> cmbRuolo = new ComboBox<>();
                cmbRuolo.getItems().addAll("TeamPrincipal", "DirettoreTecnico", "Collaudatore", "IngegnereMeccanico", "IngegnereElettronico");
                cmbRuolo.getSelectionModel().select(0);
                contentRuolo.getChildren().addAll(labelRuolo, cmbRuolo);
                // scuderia
                final HBox contentScuderia = new HBox();
                final Label labelScuderia = new Label("Scuderia: ");
                final ComboBox<String> cmbScuderia = new ComboBox<>();
                ScuderiaDAO.getScuderiaList().forEach(s -> cmbScuderia.getItems().add(s.getNome()));
                cmbScuderia.getSelectionModel().select(0);
                contentScuderia.getChildren().addAll(labelScuderia, cmbScuderia);
                
                final Button buttonCommit = new Button("Commit");
                buttonCommit.setOnAction(new EventHandler<ActionEvent>() {
                    @Override public void handle(final ActionEvent e) {
                        try {
                            if(Nome.getText().toString().trim().length() > 0 && Cognome.getText().toString().trim().length() > 0 && CF.getText().toString().trim().length() > 0) {
                                String[] elems = dataNascita.getValue().toString().split("-");
                                String dateFinal = elems[2] + "/" + elems[1] + "/" + elems[0];
                                if(cmbRuolo.getSelectionModel().getSelectedItem().toString() != "IngegnereMeccanico" && cmbRuolo.getSelectionModel().getSelectedItem().toString() != "IngegnereElettronico") {
                                    AssistenteTecnicoDAO.insertAssistenteTecnico(Nome.getText().toString(), Cognome.getText().toString(), CF.getText().toString(), dateFinal, cmbRuolo.getSelectionModel().getSelectedItem().toString(), cmbScuderia.getSelectionModel().getSelectedItem().toString());
                                } else {
                                    // ingegneri
                                    AssistenteTecnicoDAO.insertAssistenteTecnico(Nome.getText().toString(), Cognome.getText().toString(), CF.getText().toString().toUpperCase(), dateFinal, "Ingegnere", cmbScuderia.getSelectionModel().getSelectedItem().toString());
                                    String tipoIngegnere = cmbRuolo.getSelectionModel().getSelectedItem().toString() == "IngegnereMeccanico" ? "Meccanico" : "Elettronico"; 
                                    AssistenteTecnicoDAO.insertIngegnere(CF.getText().toString(), tipoIngegnere);
                                }
                                tableViewAssistenti.setItems(FXCollections.observableArrayList(AssistenteTecnicoDAO.getAssistentiList()));
                                addStage.close();
                            } else {
                                final Alert alert = new Alert(AlertType.INFORMATION);
                                alert.setTitle("Info");
                                alert.setHeaderText("Errore Inserimento Assistente");
                                alert.setContentText("Inserire campi in modo corretto");

                                alert.showAndWait();
                            }
                        } catch (NumberFormatException | ClassNotFoundException | SQLException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                        addStage.close();
                    }
                });
                
                content.getChildren().addAll(contentCF, contentNome, contentCognome, contentDataNascita, contentRuolo, contentScuderia, buttonCommit);
            }
        }
        
        addStage.setScene(stageScene);
        addStage.show();
    }
    
    @FXML
    public void elimina() throws ClassNotFoundException, SQLException {
        if(this.tableViewGiudici.getSelectionModel().getSelectedItem() != null) {
            GiudiceDAO.deleteGiuduce(this.tableViewGiudici.getSelectionModel().getSelectedItem().getCF());
            this.tableViewGiudici.setItems(FXCollections.observableArrayList(GiudiceDAO.getGiudiciList()));
        } else {
            if(this.cmbScuderia.getSelectionModel().getSelectedIndex() == 0) {
                // pilota
                if(this.tableViewPiloti.getSelectionModel().getSelectedItem() != null) {
                    PilotaDAO.deletePilota(this.tableViewPiloti.getSelectionModel().getSelectedItem().getCF());
                    this.tableViewPiloti.setItems(FXCollections.observableArrayList(PilotaDAO.getPilotaList()));
                }
            } else {
                // assistente tecnico
                if(this.tableViewAssistenti.getSelectionModel().getSelectedItem() != null) {
                    if(this.tableViewAssistenti.getSelectionModel().getSelectedItem().getTipo().equals("Ingegnere")) {
                        AssistenteTecnicoDAO.deleteIngegnere(this.tableViewAssistenti.getSelectionModel().getSelectedItem().getCF());
                    }
                    AssistenteTecnicoDAO.deleteAssistenteTecnico(this.tableViewAssistenti.getSelectionModel().getSelectedItem().getCF());
                    this.tableViewAssistenti.setItems(FXCollections.observableArrayList(AssistenteTecnicoDAO.getAssistentiList()));
                }
            }
        }
    }
    
    @FXML
    public void selezioneGiudice() {
        this.ckbGiudice.setSelected(true);
        this.ckbScuderia.setSelected(false);
        this.cmbScuderia.setDisable(true);
    }
    
    @FXML
    public void selezioneScuderia() {
        this.ckbGiudice.setSelected(false);
        this.ckbScuderia.setSelected(true);
        this.cmbScuderia.setDisable(false);
    }
    
    @FXML
    public void tornaAlMenu() {
        this.environment.displayScreen(FXMLScreens.PRINCIPAL_SCREEN);
    }
    
    public void show() {
        this.environment.displayScreen(FXMLScreens.PERSONALE);
    }
}
