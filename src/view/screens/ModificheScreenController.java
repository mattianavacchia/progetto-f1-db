package view.screens;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import controller.ModificheController;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.StringConverter;
import model.ComponenteDAO;
import model.ComponentePUDAO;
import model.ComposizionePUDAO;
import model.modifihe.EsecuzioneModificaDAO;
import model.modifihe.EsecuzioneSostituzioneDAO;
import model.modifihe.Modifica;
import model.modifihe.ModificaDAO;
import model.modifihe.Sostituzione;
import model.modifihe.SostituzioneDAO;
import model.scuderia.AssistenteTecnico;
import model.scuderia.AssistenteTecnicoDAO;
import model.scuderia.Monoposto;
import model.scuderia.MonopostoDAO;
import model.scuderia.PilotaDAO;
import view.FXEnvironment;

public class ModificheScreenController {

    private FXEnvironment environment;
    private ModificheController controller;
    
    @FXML
    Text txtMGU_H, txtMGU_K, txtElettronica, txtBatteria, txtTurbocompressore, txtMotore;
    
    @FXML
    Button btnMGU_H, btnMGU_K, btnElettronica, btnBatteria, btnTurbocompressore, btnMotore;
    
    @FXML
    ComboBox<Monoposto> cmbMonoposto;
    
    @FXML
    ComboBox<String> cmbComponentiSos, cmbComponentiMod;
    
    @FXML
    ComboBox<AssistenteTecnico> cmbIngegneriSos, cmbIngegneriMod;
    
    @FXML
    TableView<Sostituzione> tableViewSostituzioni;
    
    @FXML
    TableView<Modifica> tableViewModifiche;
    
    @FXML
    TextField txtFieldDescrizione;
    
    @FXML
    VBox vboxContenuti;
    
    @FXML
    HBox hboxAggIng;
    
    int codComponenteSos;
    Set<AssistenteTecnico> set = new HashSet<>();

    public ModificheScreenController(final FXEnvironment environment, final ModificheController controller) {
        super();
        this.environment = environment;
        this.environment.loadScreen(FXMLScreens.MODIFICHE, this);
        this.controller = controller;
    }
    
    @SuppressWarnings("unchecked")
    public void initialize() {
        
        final TableColumn<Sostituzione, String> data = new TableColumn<>("Data");
        data.setCellValueFactory(
                new PropertyValueFactory<Sostituzione, String>("data")
        );
        final TableColumn<Sostituzione, Integer> codComponenteAttuale = new TableColumn<>("Componente Attuale");
        codComponenteAttuale.setCellValueFactory(
                new PropertyValueFactory<Sostituzione, Integer>("codComponenteAttuale")
        );
        final TableColumn<Sostituzione, Integer> codNuovo = new TableColumn<>("Componente Nuovo");
        codNuovo.setCellValueFactory(
                new PropertyValueFactory<Sostituzione, Integer>("codNuovoAttuale")
        );
        this.tableViewSostituzioni.getColumns().addAll(data, codComponenteAttuale, codNuovo);
        
        final TableColumn<Modifica, String> dataM = new TableColumn<>("Data");
        dataM.setCellValueFactory(
                new PropertyValueFactory<Modifica, String>("data")
        );
        final TableColumn<Modifica, String> descrizione = new TableColumn<>("Descrizione");
        descrizione.setCellValueFactory(
                new PropertyValueFactory<Modifica, String>("descrizione")
        );
        final TableColumn<Modifica, Integer> codCompoM = new TableColumn<>("Componente");
        codCompoM.setCellValueFactory(
                new PropertyValueFactory<Modifica, Integer>("codComponente")
        );
        this.tableViewModifiche.getColumns().addAll(dataM, descrizione, codCompoM);
        
        this.cmbIngegneriSos.setCellFactory(param -> new ListCell<AssistenteTecnico>() {
            @Override
            protected void updateItem(final AssistenteTecnico item, final boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null || item.getCF() == null) {
                    setText(null);
                } else {
                    setText(item.getNome() + " " + item.getCognome());
                }
            }
        });
        
        this.cmbIngegneriSos.setConverter(new StringConverter<AssistenteTecnico>() {
            @Override
            public String toString(AssistenteTecnico item) {
                if (item == null) {
                    return null;
                } else {
                    return item.getNome() + " " + item.getCognome();
                }
            }

            @Override
            public AssistenteTecnico fromString(String personString) {
                return null; // No conversion fromString needed.
            }
        });
        
        this.cmbIngegneriMod.setCellFactory(param -> new ListCell<AssistenteTecnico>() {
            @Override
            protected void updateItem(final AssistenteTecnico item, final boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null || item.getCF() == null) {
                    setText(null);
                } else {
                    setText(item.getNome() + " " + item.getCognome());
                }
            }
        });
        
        this.cmbIngegneriMod.setConverter(new StringConverter<AssistenteTecnico>() {
            @Override
            public String toString(AssistenteTecnico item) {
                if (item == null) {
                    return null;
                } else {
                    return item.getNome() + " " + item.getCognome();
                }
            }

            @Override
            public AssistenteTecnico fromString(String personString) {
                return null; // No conversion fromString needed.
            }
        });
        
        this.cmbComponentiSos.setItems(FXCollections.observableArrayList("MGU_H", "MGU_K", "ELETTRONICA_DI_CONTROLLO", "BATTERIA", "TURBOCOMPRESSORE", "MOTORE_A_COMBUSTIONE"));
        this.cmbComponentiSos.getSelectionModel().select(0);
        this.cmbComponentiMod.setItems(FXCollections.observableArrayList("MGU_H", "MGU_K", "ELETTRONICA_DI_CONTROLLO", "BATTERIA", "TURBOCOMPRESSORE", "MOTORE_A_COMBUSTIONE"));
        this.cmbComponentiMod.getSelectionModel().select(0);
        
        this.cmbMonoposto.setCellFactory(param -> new ListCell<Monoposto>() {
            @Override
            protected void updateItem(final Monoposto item, final boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null || item.getCfPilota() == null) {
                    setText(null);
                } else {
                    setText(PilotaDAO.getPilotaFromCF(item.getCfPilota()).getNome() + " " + PilotaDAO.getPilotaFromCF(item.getCfPilota()).getCognome() + " - " + item.getScuderia());
                }
            }
        });
        
        this.cmbMonoposto.setConverter(new StringConverter<Monoposto>() {
            @Override
            public String toString(Monoposto item) {
                if (item == null) {
                    return null;
                } else {
                    return PilotaDAO.getPilotaFromCF(item.getCfPilota()).getNome() + " " + PilotaDAO.getPilotaFromCF(item.getCfPilota()).getCognome() + " - " + item.getScuderia();
                }
            }

            @Override
            public Monoposto fromString(String personString) {
                return null; // No conversion fromString needed.
            }
        });
        
        this.cmbMonoposto.setItems(FXCollections.observableArrayList(MonopostoDAO.getMonopostoList()));
        this.cmbMonoposto.getSelectionModel().select(0);
    }
    
    @FXML
    public void caricaMonoposto() {
        //System.out.println(ComponentePUDAO.checkMGU_H(ComposizionePUDAO.codComponentiPU(ComponenteDAO.codComponenteFromCF(this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota()))));
        int codComponente = ComponenteDAO.codComponenteFromCF(this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota());
        if(codComponente != -1) {
            //MGU_H
            if(ComponentePUDAO.checkPresenzaComponente(ComposizionePUDAO.codComponentiPU(ComponenteDAO.codComponenteFromCF(this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota())), "MGU_H")) {
                this.txtMGU_H.setText("Componente MGU_H: OK");
                this.txtMGU_H.setFill(Color.GREEN);
                this.btnMGU_H.setDisable(true);
            } else {
                this.txtMGU_H.setText("Componente MGU_H: NON DISPONIBILE");
                this.txtMGU_H.setFill(Color.RED);
                this.btnMGU_H.setDisable(false);
            }
            //MGU_K
            if(ComponentePUDAO.checkPresenzaComponente(ComposizionePUDAO.codComponentiPU(ComponenteDAO.codComponenteFromCF(this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota())), "MGU_K")) {
                this.txtMGU_K.setText("Componente MGU_K: OK");
                this.txtMGU_K.setFill(Color.GREEN);
                this.btnMGU_K.setDisable(true);
            } else {
                this.txtMGU_K.setText("Componente MGU_K: NON DISPONIBILE");
                this.txtMGU_K.setFill(Color.RED);
                this.btnMGU_K.setDisable(false);
            }
            //ELETTRONICA
            if(ComponentePUDAO.checkPresenzaComponente(ComposizionePUDAO.codComponentiPU(ComponenteDAO.codComponenteFromCF(this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota())), "ELETTRONICA_DI_CONTROLLO")) {
                this.txtElettronica.setText("Componente ELETTRONICA DI CONTROLLO: OK");
                this.txtElettronica.setFill(Color.GREEN);
                this.btnElettronica.setDisable(true);
            } else {
                this.txtElettronica.setText("Componente ELETTRONICA DI CONTROLLO: NON DISPONIBILE");
                this.txtElettronica.setFill(Color.RED);
                this.btnElettronica.setDisable(false);
            }
            //BATTERIA
            if(ComponentePUDAO.checkPresenzaComponente(ComposizionePUDAO.codComponentiPU(ComponenteDAO.codComponenteFromCF(this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota())), "BATTERIA")) {
                this.txtBatteria.setText("Componente BATTERIA: OK");
                this.txtBatteria.setFill(Color.GREEN);
                this.btnBatteria.setDisable(true);
            } else {
                this.txtBatteria.setText("Componente BATTERIA: NON DISPONIBILE");
                this.txtBatteria.setFill(Color.RED);
                this.btnBatteria.setDisable(false);
            }
            //TURBOCOMPRESSORE
            if(ComponentePUDAO.checkPresenzaComponente(ComposizionePUDAO.codComponentiPU(ComponenteDAO.codComponenteFromCF(this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota())), "TURBOCOMPRESSORE")) {
                this.txtTurbocompressore.setText("Componente TURBOCOMPRESSORE: OK");
                this.txtTurbocompressore.setFill(Color.GREEN);
                this.btnTurbocompressore.setDisable(true);
            } else {
                this.txtTurbocompressore.setText("Componente TURBOCOMPRESSORE: NON DISPONIBILE");
                this.txtTurbocompressore.setFill(Color.RED);
                this.btnTurbocompressore.setDisable(false);
            }
            //MOTORE
            if(ComponentePUDAO.checkPresenzaComponente(ComposizionePUDAO.codComponentiPU(ComponenteDAO.codComponenteFromCF(this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota())), "MOTORE_A_COMBUSTIONE")) {
                this.txtMotore.setText("Componente MOTORE A COMBUSTIONE: OK");
                this.txtMotore.setFill(Color.GREEN);
                this.btnMotore.setDisable(true);
            } else {
                this.txtMotore.setText("Componente MOTORE A COMBUSTIONE: NON DISPONIBILE");
                this.txtMotore.setFill(Color.RED);
                this.btnMotore.setDisable(false);
            }
        } else {
            System.out.println("NON ESISTE");
        }
        
        this.cmbIngegneriSos.setItems(FXCollections.observableArrayList(AssistenteTecnicoDAO.getAssistentiIngegneriList(this.cmbMonoposto.getSelectionModel().getSelectedItem().getScuderia())));
        this.cmbIngegneriSos.getSelectionModel().select(0);
        this.cmbIngegneriMod.setItems(FXCollections.observableArrayList(AssistenteTecnicoDAO.getAssistentiIngegneriList(this.cmbMonoposto.getSelectionModel().getSelectedItem().getScuderia())));
        this.cmbIngegneriMod.getSelectionModel().select(0);
        
        this.vboxContenuti.setVisible(true);
        this.tableViewModifiche.setItems(FXCollections.observableArrayList(ModificaDAO.getModificheList()));
        this.tableViewSostituzioni.setItems(FXCollections.observableArrayList(SostituzioneDAO.getSostituzioniList()));
    }
    
    @FXML
    public void creaMGU_K() throws ClassNotFoundException, SQLException {
        int codPU = ComponenteDAO.codComponenteFromCF((this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota()));
        int codComponentePU = ComponentePUDAO.creaComponente("MGU_K");
        ComposizionePUDAO.creaComposizione(codPU, codComponentePU);
        this.caricaMonoposto();
    }
    
    @FXML
    public void creaMGU_H() throws ClassNotFoundException, SQLException {
        int codPU = ComponenteDAO.codComponenteFromCF((this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota()));
        int codComponentePU = ComponentePUDAO.creaComponente("MGU_H");
        ComposizionePUDAO.creaComposizione(codPU, codComponentePU);
        this.caricaMonoposto();
    }
    
    @FXML
    public void creaElettronica() throws ClassNotFoundException, SQLException {
        int codPU = ComponenteDAO.codComponenteFromCF((this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota()));
        int codComponentePU = ComponentePUDAO.creaComponente("ELETTRONICA_DI_CONTROLLO");
        ComposizionePUDAO.creaComposizione(codPU, codComponentePU);
        this.caricaMonoposto();
    }
    
    @FXML
    public void creaBatteria() throws ClassNotFoundException, SQLException {
        int codPU = ComponenteDAO.codComponenteFromCF((this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota()));
        int codComponentePU = ComponentePUDAO.creaComponente("BATTERIA");
        ComposizionePUDAO.creaComposizione(codPU, codComponentePU);
        this.caricaMonoposto();
    }
    
    @FXML
    public void creaMotore() throws ClassNotFoundException, SQLException {
        int codPU = ComponenteDAO.codComponenteFromCF((this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota()));
        int codComponentePU = ComponentePUDAO.creaComponente("MOTORE_A_COMBUSTIONE");
        ComposizionePUDAO.creaComposizione(codPU, codComponentePU);
        this.caricaMonoposto();
    }
    
    @FXML
    public void creaTurbocompressore() throws ClassNotFoundException, SQLException {
        int codPU = ComponenteDAO.codComponenteFromCF((this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota()));
        int codComponentePU = ComponentePUDAO.creaComponente("TURBOCOMPRESSORE");
        ComposizionePUDAO.creaComposizione(codPU, codComponentePU);
        this.caricaMonoposto();
    }
    
    @FXML
    public void creaComponenteSostitutivo() throws ClassNotFoundException, SQLException {
        String tipo = this.cmbComponentiSos.getSelectionModel().getSelectedItem().toString();
        this.codComponenteSos = ComponentePUDAO.creaComponente(tipo);
        this.caricaMonoposto();
        this.hboxAggIng.setDisable(false);
    }
    
    @FXML
    public void addIngegnereSos() {
        this.set.add(this.cmbIngegneriSos.getSelectionModel().getSelectedItem());
    }
    
    @FXML
    public void sostituisci() throws ClassNotFoundException, SQLException {
        int attuale = ComponentePUDAO.codComponente(ComposizionePUDAO.codComponentiPU(ComponenteDAO.codComponenteFromCF(this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota())), this.cmbComponentiSos.getSelectionModel().getSelectedItem().toString());
        if(attuale != -1) {
            ComposizionePUDAO.eliminaComposizione(ComponentePUDAO.codComponente(ComposizionePUDAO.codComponentiPU(ComponenteDAO.codComponenteFromCF(this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota())), this.cmbComponentiSos.getSelectionModel().getSelectedItem().toString()));
            int codPU = ComponenteDAO.codComponenteFromCF((this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota()));
            ComposizionePUDAO.creaComposizione(codPU, this.codComponenteSos);
            // crea sos
            Date date = new Date();
            String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
            String[] elems = modifiedDate.split("-");
            String dateFinal = elems[2] + "/" + elems[1] + "/" + elems[0];
            System.out.println(dateFinal);
            SostituzioneDAO.creaSos(dateFinal, attuale, this.codComponenteSos);
            // metti ingsos
            for (AssistenteTecnico ass : this.set) {
                EsecuzioneSostituzioneDAO.eseguiSos(dateFinal, attuale, ass.getCF());
            }
            this.set.clear();
            this.tableViewSostituzioni.setItems(FXCollections.observableArrayList(SostituzioneDAO.getSostituzioniList()));
        } else {
            final Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Info");
            alert.setHeaderText("Errore Modifica");
            alert.setContentText("Componente non presente.");

            alert.showAndWait();
        }
    }
    
    @FXML
    public void modifica() throws ClassNotFoundException, SQLException {
        int attuale = ComponentePUDAO.codComponente(ComposizionePUDAO.codComponentiPU(ComponenteDAO.codComponenteFromCF(this.cmbMonoposto.getSelectionModel().getSelectedItem().getCfPilota())), this.cmbComponentiSos.getSelectionModel().getSelectedItem().toString());
        if(attuale != -1) {
            String desc = this.txtFieldDescrizione.getText().toString();
            if(desc.trim().length() > 0) {
                // crea sos
                Date date = new Date();
                String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
                String[] elems = modifiedDate.split("-");
                String dateFinal = elems[2] + "/" + elems[1] + "/" + elems[0];
                String hour = "" + LocalDateTime.now().getHour() + ":" + LocalDateTime.now().getMinute() + ":" + LocalDateTime.now().getSecond();
                ModificaDAO.creaMod(dateFinal, hour, desc, attuale);
                // metti ingsos
                for (AssistenteTecnico ass : this.set) {
                    EsecuzioneModificaDAO.eseguiMod(dateFinal, hour, attuale, ass.getCF());
                }
                this.set.clear();
                this.tableViewModifiche.setItems(FXCollections.observableArrayList(ModificaDAO.getModificheList()));
            } else {
                final Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Info");
                alert.setHeaderText("Errore Descrizione");
                alert.setContentText("Inserire descrizione adeguata.");

                alert.showAndWait();
            }
        } else {
            final Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Info");
            alert.setHeaderText("Errore Modifica");
            alert.setContentText("Componente non presente.");

            alert.showAndWait();
        }
    }
    
    public void show() {
        this.environment.displayScreen(FXMLScreens.MODIFICHE);
    }
    
    @FXML
    public void tornaAlMenu() {
        this.environment.displayScreen(FXMLScreens.PRINCIPAL_SCREEN);
    }
}