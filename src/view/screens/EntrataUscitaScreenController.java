package view.screens;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import controller.EntrataUscitaController;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import model.scuderia.EntrataUscitaDAO;
import model.scuderia.Pilota;
import model.scuderia.PilotaDAO;
import model.scuderia.Scuderia;
import model.scuderia.ScuderiaDAO;
import view.FXEnvironment;

public class EntrataUscitaScreenController {
    private FXEnvironment environment;
    private EntrataUscitaController controller;
    
    @FXML
    CheckBox ckbEntrata, ckbUscita;
    
    @FXML
    VBox vboxContenuti, vboxPiloti;
    
    @FXML
    ComboBox<Scuderia> cmbScuderia;
    
    @FXML
    ComboBox<Pilota> cmbPilota;

    public EntrataUscitaScreenController(final FXEnvironment environment, final EntrataUscitaController controller) {
        super();
        this.environment = environment;
        this.environment.loadScreen(FXMLScreens.ENTRATAUSCITA, this);
        this.controller = controller;
    }
    
    public void initialize() {
        
        this.cmbScuderia.setCellFactory(param -> new ListCell<Scuderia>() {
            @Override
            protected void updateItem(final Scuderia item, final boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null || item.getNome() == null) {
                    setText(null);
                } else {
                    setText(item.getNome());
                }
            }
        });
        
        this.cmbScuderia.setConverter(new StringConverter<Scuderia>() {
            @Override
            public String toString(Scuderia person) {
                if (person == null) {
                    return null;
                } else {
                    return person.getNome();
                }
            }

            @Override
            public Scuderia fromString(String personString) {
                return null; // No conversion fromString needed.
            }
        });
        
        this.cmbPilota.setCellFactory(param -> new ListCell<Pilota>() {
            @Override
            protected void updateItem(final Pilota item, final boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null || item.getNome() == null || item.getCognome() == null) {
                    setText(null);
                } else {
                    setText(item.getNome() + " " + item.getCognome());
                }
            }
        });
        
        this.cmbPilota.setConverter(new StringConverter<Pilota>() {
            @Override
            public String toString(Pilota pilota) {
                if (pilota == null) {
                    return null;
                } else {
                    return pilota.getNome() + " " + pilota.getCognome();
                }
            }

            @Override
            public Pilota fromString(String personString) {
                return null; // No conversion fromString needed.
            }
        });
    }
    
    @FXML
    public void carica() {
        this.vboxContenuti.setVisible(true);
        this.cmbScuderia.setItems(FXCollections.observableArrayList(ScuderiaDAO.getScuderiaList()));
        this.cmbScuderia.getSelectionModel().select(0);
        this.cmbPilota.setItems(FXCollections.observableArrayList(PilotaDAO.getPilotaList()));
        this.cmbPilota.getSelectionModel().select(0);
        this.vboxPiloti.setVisible(true);
    }
    
    @FXML
    public void caricaPilota() {
    }
    
    @FXML
    public void aggiorna() throws ClassNotFoundException, SQLException {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        String[] elems = modifiedDate.split("-");
        String dateFinal = elems[2] + "/" + elems[1] + "/" + elems[0];
        
        if(this.ckbEntrata.isSelected()) {
            // entrata
            if(EntrataUscitaDAO.checkE(this.cmbPilota.getSelectionModel().getSelectedItem().getCF())) {
                EntrataUscitaDAO.insertMovement(this.cmbPilota.getSelectionModel().getSelectedItem().getCF(), this.cmbScuderia.getSelectionModel().getSelectedItem().getNome(), dateFinal, "E");
            } else {
                final Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Info");
                alert.setHeaderText("Errore Aggiornamento");
                alert.setContentText("Il pilota scelto deve prima uscire dalla sua squadra attuale.");

                alert.showAndWait();
            }
        } else {
            // uscita
            if(EntrataUscitaDAO.checkU(this.cmbPilota.getSelectionModel().getSelectedItem().getCF(), this.cmbScuderia.getSelectionModel().getSelectedItem().getNome())) {
                EntrataUscitaDAO.insertMovement(this.cmbPilota.getSelectionModel().getSelectedItem().getCF(), this.cmbScuderia.getSelectionModel().getSelectedItem().getNome(), dateFinal, "U");
            } else {
                final Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Info");
                alert.setHeaderText("Errore Aggiornamento");
                alert.setContentText("Il pilota scelto deve prima entrare nella squadra.");

                alert.showAndWait();
            }
        }
    }
    
    @FXML
    public void sceltaEntrata() {
        this.ckbEntrata.setSelected(true);
        this.ckbUscita.setSelected(false);
    }
    
    @FXML
    public void sceltaUscita() {
        this.ckbEntrata.setSelected(false);
        this.ckbUscita.setSelected(true);
    }
    
    @FXML
    public void tornaAlMenu() {
        this.environment.displayScreen(FXMLScreens.PRINCIPAL_SCREEN);
    }
    
    public void show() {
        this.environment.displayScreen(FXMLScreens.ENTRATAUSCITA);
    }
}