package view.screens;

import java.sql.SQLException;

import controller.PenalitaController;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import model.campionato.CorpoGaraDAO;
import model.campionato.PenalitaPilota;
import model.campionato.PenalitaPilotaDAO;
import model.campionato.PenalitaScuderia;
import model.campionato.PenalitaScuderiaDAO;
import model.scuderia.Pilota;
import model.scuderia.PilotaDAO;
import model.scuderia.Scuderia;
import model.scuderia.ScuderiaDAO;
import view.FXEnvironment;

public class PenalitaScreenController {

    private FXEnvironment environment;
    private PenalitaController controller;
    
    @FXML
    public CheckBox ckboxScuderia, ckboxPilota;
    
    @FXML
    public TableView<PenalitaScuderia> tableViewScuderia;
    
    @FXML
    public TableView<PenalitaPilota> tableViewPilota;
    
    @FXML
    HBox hboxAzioni;

    public PenalitaScreenController(final FXEnvironment environment, final PenalitaController controller) {
        super();
        this.environment = environment;
        this.environment.loadScreen(FXMLScreens.PENALITA, this);
        this.controller = controller;
    }
    
    @SuppressWarnings("unchecked")
    public void initialize() {
        /**********************
         * PENALITA PILOTI
         * *******************/
        final TableColumn<PenalitaPilota, String> descrizionePilota = new TableColumn<>("Descrizione");
        descrizionePilota.setCellValueFactory(
                new PropertyValueFactory<PenalitaPilota, String>("descrizione")
        );
        final TableColumn<PenalitaPilota, String> cfPilota = new TableColumn<>("Pilota");
        cfPilota.setCellValueFactory(
            new PropertyValueFactory<PenalitaPilota, String>("CFPilota")
        );
        final TableColumn<PenalitaPilota, Integer>  cgPilota = new TableColumn<>("Corpo Gara");
        cgPilota.setCellValueFactory(
            new PropertyValueFactory<PenalitaPilota, Integer>("codCorpoGara")
        );
        this.tableViewPilota.getColumns().addAll(descrizionePilota, cfPilota, cgPilota);
        
        /**********************
         * PENALITA SCUDERIA
         * *******************/
        final TableColumn<PenalitaScuderia, String> descrizioneScuderia = new TableColumn<>("Descrizione");
        descrizioneScuderia.setCellValueFactory(
                new PropertyValueFactory<PenalitaScuderia, String>("descrizione")
        );
        final TableColumn<PenalitaScuderia, String> nomeScuderia = new TableColumn<>("Scuderia");
        nomeScuderia.setCellValueFactory(
                new PropertyValueFactory<PenalitaScuderia, String>("Scuderia")
        );
        final TableColumn<PenalitaScuderia, Integer> cgScuderia = new TableColumn<>("Corpo Gara");
        cgScuderia.setCellValueFactory(
                new PropertyValueFactory<PenalitaScuderia, Integer>("codCorpoGara")
        );
        this.tableViewScuderia.getColumns().addAll(descrizioneScuderia, nomeScuderia, cgScuderia);
    }
    
    public void show() {
        this.environment.displayScreen(FXMLScreens.PENALITA);
    }
    
    @FXML
    public void aggiungi() {
        final Stage addStage = new Stage();
        final VBox content = new VBox();
        Scene stageScene = new Scene(content, 300, 270);
        
        // Descrizione
        final HBox contentDescrizione = new HBox();
        final Label labelDescrizione = new Label("Descrizione: ");
        final TextArea Descrizione = new TextArea();
        Descrizione.setMaxWidth(200);
        contentDescrizione.getChildren().addAll(labelDescrizione, Descrizione);
        
        // Corpo Gara
        final HBox contentCorpoGara = new HBox();
        final Label labelCorpoGara = new Label("Corpo Gara: ");
        final ComboBox<Integer> CorpoGara = new ComboBox<>();
        CorpoGaraDAO.getCorpoGaraList().forEach(c -> CorpoGara.getItems().add(c.getCodCorpoGara()));
        CorpoGara.getSelectionModel().select(0);
        contentCorpoGara.getChildren().addAll(labelCorpoGara, CorpoGara);
        
        if(this.ckboxPilota.isSelected()) {
            // pilota
            final HBox contentPilota = new HBox();
            final Label labelPilota = new Label("Pilota: ");
            final ComboBox<Pilota> Pilota = new ComboBox<>();
            Pilota.setCellFactory(param -> new ListCell<Pilota>() {
                @Override
                protected void updateItem(final Pilota item, final boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null || item.getNome() == null || item.getCognome() == null) {
                        setText(null);
                    } else {
                        setText(item.getNome() + " " + item.getCognome());
                    }
                }
            });
            Pilota.setConverter(new StringConverter<Pilota>() {
                @Override
                public String toString(Pilota pilota) {
                    if (pilota == null) {
                        return null;
                    } else {
                        return pilota.getNome() + " " + pilota.getCognome();
                    }
                }

                @Override
                public Pilota fromString(String personString) {
                    return null; // No conversion fromString needed.
                }
            });
            
            Pilota.getItems().addAll(PilotaDAO.getPilotaList());
            Pilota.getSelectionModel().select(0);
            contentPilota.getChildren().addAll(labelPilota, Pilota);
            
            final Button buttonCommit = new Button("Commit");
            buttonCommit.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(final ActionEvent e) {
                    try {
                        if(Descrizione.getText().toString().trim().length() > 0) {
                            PenalitaPilotaDAO.insertPenalita(Descrizione.getText().toString(), Pilota.getSelectionModel().getSelectedItem().getCF(), CorpoGara.getSelectionModel().getSelectedItem().intValue());
                            tableViewPilota.setItems(FXCollections.observableArrayList(PenalitaPilotaDAO.getPenalitaPilotiList()));
                            addStage.close();
                        } else {
                            final Alert alert = new Alert(AlertType.INFORMATION);
                            alert.setTitle("Info");
                            alert.setHeaderText("Errore Descrizione Penalità");
                            alert.setContentText("Inserisci un testo adeguato.");

                            alert.showAndWait();
                        }
                    } catch (ClassNotFoundException | SQLException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            });
            
            content.getChildren().addAll(contentDescrizione, contentCorpoGara, contentPilota, buttonCommit);
        } else {
            // scuderia
            final HBox contentScuderia = new HBox();
            final Label labelScuderia = new Label("Scuderia: ");
            final ComboBox<Scuderia> Scuderia = new ComboBox<>();
            Scuderia.setCellFactory(param -> new ListCell<Scuderia>() {
                @Override
                protected void updateItem(final Scuderia item, final boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null || item.getNome() == null) {
                        setText(null);
                    } else {
                        setText(item.getNome());
                    }
                }
            });
            Scuderia.setConverter(new StringConverter<Scuderia>() {
                @Override
                public String toString(Scuderia person) {
                    if (person == null) {
                        return null;
                    } else {
                        return person.getNome();
                    }
                }

                @Override
                public Scuderia fromString(String personString) {
                    return null; // No conversion fromString needed.
                }
            });
        
            Scuderia.getItems().addAll(ScuderiaDAO.getScuderiaList());
            Scuderia.getSelectionModel().select(0);
            contentScuderia.getChildren().addAll(labelScuderia, Scuderia);
            
            final Button buttonCommit = new Button("Commit");
            buttonCommit.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(final ActionEvent e) {
                    try {
                        if(Descrizione.getText().toString().trim().length() > 0) {
                            PenalitaScuderiaDAO.insertPenalita(Descrizione.getText().toString(), Scuderia.getSelectionModel().getSelectedItem().getNome(), CorpoGara.getSelectionModel().getSelectedItem().intValue());
                            tableViewScuderia.setItems(FXCollections.observableArrayList(PenalitaScuderiaDAO.getPenalitaScuderieList()));
                            addStage.close();
                        } else {
                            final Alert alert = new Alert(AlertType.INFORMATION);
                            alert.setTitle("Info");
                            alert.setHeaderText("Errore Descrizione Penalità");
                            alert.setContentText("Inserisci un testo adeguato.");

                            alert.showAndWait();
                        }
                    } catch (ClassNotFoundException | SQLException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            });
            
            content.getChildren().addAll(contentDescrizione, contentCorpoGara, contentScuderia, buttonCommit);
        }
        
        addStage.setScene(stageScene);
        addStage.show();
    }
    
    @FXML
    public void elimina() throws ClassNotFoundException, SQLException {
        if(this.ckboxPilota.isSelected()) {
            // pilota
            if(this.tableViewPilota.getSelectionModel().getSelectedItem() != null) {
                PenalitaPilotaDAO.deletePenalita(this.tableViewPilota.getSelectionModel().getSelectedItem().getCodPenalita());
                this.tableViewPilota.setItems(FXCollections.observableArrayList(PenalitaPilotaDAO.getPenalitaPilotiList()));
            }
        } else {
            // scuderia
            if(this.tableViewScuderia.getSelectionModel().getSelectedItem() != null) {
                PenalitaScuderiaDAO.deletePenalita(this.tableViewScuderia.getSelectionModel().getSelectedItem().getCodPenalita());
                this.tableViewScuderia.setItems(FXCollections.observableArrayList(PenalitaScuderiaDAO.getPenalitaScuderieList()));
            }
        }
    }
    
    @FXML
    public void operazioneScuderia() {
        this.ckboxPilota.setSelected(false);
        this.ckboxScuderia.setSelected(true);
    }
    
    @FXML
    public void operazionePilota() {
        this.ckboxPilota.setSelected(true);
        this.ckboxScuderia.setSelected(false);
    }
    
    @FXML
    public void caricaScuderiaPilota() {
        if(this.ckboxPilota.isSelected()) {
            this.tableViewPilota.setVisible(true);
            this.tableViewScuderia.setVisible(false);
            this.tableViewPilota.setItems(FXCollections.observableArrayList(PenalitaPilotaDAO.getPenalitaPilotiList()));
        } else {
            this.tableViewPilota.setVisible(false);
            this.tableViewScuderia.setVisible(true);
            this.tableViewScuderia.setItems(FXCollections.observableArrayList(PenalitaScuderiaDAO.getPenalitaScuderieList()));
        }
        
        hboxAzioni.setVisible(true);
    }
    
    @FXML
    public void tornaAlMenu() {
        this.environment.displayScreen(FXMLScreens.PRINCIPAL_SCREEN);
    }
}
