package view.screens;

import controller.PrincipalController;
import javafx.fxml.FXML;
import view.FXEnvironment;

public class PrincipalScreenController {

    private FXEnvironment environment;
    private PrincipalController controller;

    public PrincipalScreenController(final FXEnvironment environment, final PrincipalController rlfController) {
        super();
        this.environment = environment;
        this.environment.loadScreen(FXMLScreens.PRINCIPAL_SCREEN, this);
        this.controller = rlfController;
    }
    
    public void show() {
        this.environment.displayScreen(FXMLScreens.PRINCIPAL_SCREEN);
    }
    
    @FXML
    public void goToPenalita() {
        this.environment.displayScreen(FXMLScreens.PENALITA);
    }
    
    @FXML
    public void goToModifiche() {
        this.environment.displayScreen(FXMLScreens.MODIFICHE);
    }
    
    @FXML
    public void goToGara() {
        this.environment.displayScreen(FXMLScreens.GARA);
    }
    
    @FXML
    public void goToPersonale() {
        this.environment.displayScreen(FXMLScreens.PERSONALE);
    }
    
    @FXML
    public void goToEntrataUscita() {
        this.environment.displayScreen(FXMLScreens.ENTRATAUSCITA);
    }
    
    @FXML
    public void goToMonoposto() {
        this.environment.displayScreen(FXMLScreens.MONOPOSTO);
    }
    
    @FXML
    public void goToClassifica() {
        this.environment.displayScreen(FXMLScreens.CLASSIFICA);
    }
}
